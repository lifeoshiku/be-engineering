# coding=utf8
from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import UserManager
from django.utils import six, timezone
from django.contrib.auth.validators import ASCIIUsernameValidator, UnicodeUsernameValidator
from django.utils.translation import ugettext_lazy as _
from app_settings.models import Department
from course.models import CourseVideo


def limit_public_course():
    return {
        'public': True
    }


def user_directory_path(instance, filename):
    return 'uploads/user_{0}/{1}'.format(instance.id, filename)


class Member(AbstractBaseUser):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username and password are required. Other fields are optional.
    """
    username_validator = UnicodeUsernameValidator() if six.PY3 else ASCIIUsernameValidator()

    username = models.CharField(
        _('username'),
        null=True,
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    first_name = models.CharField(_('first name'), max_length=128, blank=True)
    last_name = models.CharField(_('last name'), max_length=128, blank=True)
    nick_name = models.CharField(u'ชื่อเล่น', max_length=30, blank=True)
    gender = models.CharField(u'เพศ', max_length=30, blank=True)
    mobile = models.CharField(u'เบอร์โทรศัพท์', max_length=30, blank=True)
    email = models.EmailField(_('email address'), blank=True)
    facebook = models.CharField('facebook', max_length=128, blank=True)
    address = models.TextField('ที่อยู่', max_length=512, blank=True)
    picture = models.ImageField(verbose_name=u'รูปโปรไฟล์', upload_to=user_directory_path, null=True, default=None)
    department = models.ForeignKey('app_settings.Department', null=True, default=None, verbose_name=u'ภาควิชา')
    gpax = models.CharField('GPAX', max_length=4, blank=True)
    known_form = models.CharField('รู้จักจาก', max_length=256, blank=True)
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def clean(self):
        super(Member, self).clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = u'%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        "Returns the short name for the user."
        return self.first_name

    def __str__(self):
        return self.get_username() or ''

    def __unicode__(self):
        return self.get_username() or ''

    class Meta:
        verbose_name = u'นักเรียน'
        verbose_name_plural = u'นักเรียน'


class CourseMember(models.Model):
    member = models.ForeignKey(
        'member.Member',
        null=False,
        db_index=True,
    )
    course = models.ForeignKey(
        'course.Course',
        limit_choices_to=limit_public_course,
        null=False,
        verbose_name=u'วิชาเรียน',
    )
    remaining_studying_hours = models.PositiveIntegerField(
        default=0,
        null=False,
        blank=False,
        verbose_name=u'ชั่วโมงเรียนคงเหลือ',
    )
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    def save(self, *args, **kwargs):
        if not self.pk:
            super(self.__class__, self).save(*args, **kwargs)
            stt = StudyingTimeTransaction(
                member=self.member,
                course=self,
                studying_hours=self.course.default_hours,
                action=TransactionActionEnum.INITIALIZE,
                reason=None,
            )
            stt.save()
        else:
            super(self.__class__, self).save(*args, **kwargs)

    def __str__(self):
        return u'%s (%d ชั่วโมง)' % (self.course, self.remaining_studying_hours, )

    def __unicode__(self):
        return u'%s (%d ชั่วโมง)' % (self.course, self.remaining_studying_hours, )

    class Meta:
        ordering = ('course',)
        verbose_name = u'รายวิชาที่ลงทะเบียนเรียน'
        verbose_name_plural = verbose_name
        unique_together = (('member', 'course',),)


class CourseUsage(CourseMember):

    class Meta:
        proxy = True
        ordering = ('-last_updated',)
        verbose_name = u'เวลาคงเหลือสำหรับแต่ละรายวิชา'
        verbose_name_plural = verbose_name


class TransactionActionEnum:
    INITIALIZE = 'INITIALIZE'
    MANUAL_INCREASE = 'MANUAL_INCREASE'
    MANUAL_DECREASE = 'MANUAL_DECREASE'
    BOOKING = 'BOOKING'
    CANCEL_BOOKING = 'CANCEL_BOOKING'



TRANSACTION_ACTION_CHOICES = (
    (TransactionActionEnum.INITIALIZE, TransactionActionEnum.INITIALIZE,),
    (TransactionActionEnum.MANUAL_DECREASE, TransactionActionEnum.MANUAL_DECREASE,),
    (TransactionActionEnum.MANUAL_INCREASE, TransactionActionEnum.MANUAL_INCREASE,),
    (TransactionActionEnum.BOOKING, TransactionActionEnum.BOOKING,),
    (TransactionActionEnum.CANCEL_BOOKING, TransactionActionEnum.CANCEL_BOOKING,),
)


class StudyingTimeTransaction(models.Model):
    member = models.ForeignKey(
        'member.Member',
        null=False,
        verbose_name=u'นักเรียน',
    )
    course = models.ForeignKey(
        'member.CourseMember',
        null=False,
        verbose_name=u'วิชาเรียน',
    )
    studying_hours = models.IntegerField(
        null=False,
        verbose_name=u'ชั่วโมงเรียน (มากกว่า 0)',
    )
    action = models.CharField(
        max_length=32,
        null=False,
        default='',
        choices=TRANSACTION_ACTION_CHOICES,
        verbose_name=u'เพิ่ม/ลด',
    )
    reason = models.TextField(
        max_length=256,
        null=True,
        blank=True,
        verbose_name=u'เหตุผล',
    )
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    def save(self, *args, **kwargs):
        super(StudyingTimeTransaction, self).save(*args, **kwargs)
        sum_hour = StudyingTimeTransaction.objects.filter(course=self.course).aggregate(models.Sum('studying_hours'))
        self.course.remaining_studying_hours = sum_hour['studying_hours__sum']
        self.course.save()


class SLSLoginLog(models.Model):
    booking = models.ForeignKey(
        'booking.Booking',
        null=False,
        verbose_name=u'Booking',
    )
    login_datetime = models.DateTimeField(
        auto_now_add=True,
        editable=False
    )


class SLSCourseVideoViewHistory(models.Model):
    member = models.ForeignKey(
        'member.Member',
        null=False,
        verbose_name=u'นักเรียน',
    )
    course_video = models.ForeignKey(
        'course.CourseVideo',
        null=False,
        verbose_name=u'วีดีโอที่เรียน',
    )
    created = models.DateTimeField(auto_now_add=True, editable=False)
