from django.shortcuts import render
from dal import autocomplete
from .models import Member, CourseMember
from django.db.models import Q


class MemberAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated():
            return Member.objects.none()

        qs = Member.objects.all()

        if self.q:
            query = Q(first_name__icontains=self.q)
            query |= Q(last_name__icontains=self.q)
            query |= Q(nick_name__icontains=self.q)
            query |= Q(username__icontains=self.q)
            qs = qs.filter(query)

        return qs


class CourseMemberAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated():
            return CourseMember.objects.none()

        member = self.forwarded.get('member', None)
        if not member:
            return CourseMember.objects.none()

        qs = CourseMember.objects.filter(member=member)

        if self.q:
            query = Q(course__name__icontains=self.q)
            qs = qs.filter(query)

        return qs