from django.core.management.base import BaseCommand, CommandError
from booking.models import BookingActionEnum, BookingPeriod, Booking
from app_settings.models import SettingProfile
from member.models import SLSLoginLog
from datetime import datetime, time, timedelta
from booking.logic import get_current_period


class Command(BaseCommand):
    help = 'Cancel all not logged slot in SLS'

    # logic
    # if time more than or equal to checking time,
    # get list of booking in current period and
    # get list of logged in in current period.
    # if some booking not loggin in between early_login to checking time,
    # cancel that booking
    def handle(self, *args, **options):
        now = datetime.now()
        current_period = get_current_period(now)
        if not current_period:
            return

        setting_profile = SettingProfile.get_default()
        late_login_time = timedelta(
            minutes=setting_profile.sls_clear_booking_status_minute
        )

        start_datetime = datetime.combine(now.date(), current_period.start_time)
        late_login_datetime = start_datetime + late_login_time

        if now < late_login_datetime:
            return

        booking_periods = BookingPeriod.objects.filter(
            booking__date=now.date(),
            booking__action=BookingActionEnum.BOOKED,
            period=current_period,
        ).values_list('booking', flat=True)

        early_login_time = timedelta(
            minutes=setting_profile.sls_early_login_time
        )
        early_login_datetime = start_datetime - early_login_time

        loggedin_list = SLSLoginLog.objects.filter(
            login_datetime__gte=early_login_datetime,
            login_datetime__lte=late_login_datetime,
        ).values_list('booking', flat=True)

        not_logged_in_list = [booking for booking in booking_periods if booking not in loggedin_list]
        for booking_id in not_logged_in_list:
            try:
                booking = Booking.objects.get(pk=booking_id)
                if booking.action == BookingActionEnum.BOOKED:
                    booking.action = BookingActionEnum.CANCEL_LATE_LOGIN
                    booking.save()
            except Booking.DoesNotExist as e:
                pass