# coding=utf8

from django.apps import AppConfig


class MemberConfig(AppConfig):
    name = 'member'
    verbose_name = u'นักเรียน'
