# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-07-21 17:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('member', '0005_auto_20170704_2302'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coursemember',
            name='course',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='course.Course'),
        ),
        migrations.AlterField(
            model_name='coursemember',
            name='remaining_studying_hours',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='member',
            name='department',
            field=models.ForeignKey(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='app_settings.Department'),
        ),
    ]
