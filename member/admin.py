# coding=utf8
from .models import (
    Member,
    CourseMember,
    CourseUsage,
)
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from custom_admin.sites import site
from django.shortcuts import redirect, reverse
from import_export import resources
from import_export.admin import ExportMixin
from django import forms


class CourseMemberInlineAdmin(admin.TabularInline):
    model = CourseMember
    extra = 1
    readonly_fields = ['remaining_studying_hours', ]


class MemberResource(resources.ModelResource):

    def dehydrate_department(self, obj):
        if obj.department:
            return u'%s' % (obj.department.name)

        return u'-'

    class Meta:
        model = Member
        exclude = ('password', 'picture', )


class MemberAdmin(ExportMixin, UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password', 'is_active')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'nick_name',
                              'gender', 'picture_preview', 'picture')}),
        (_('Education Detail'), {'fields': ('department', 'gpax', )}),
        (_('Contacts'), {'fields': ('mobile', 'email', 'facebook', 'address', )}),
        (_('ETC'), {'fields': ('known_form', )}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    readonly_fields = ['last_login', 'date_joined', 'picture_preview', 'custom_course_list', ]
    list_display = ('custom_name', 'department', 'mobile', 'custom_course_list', 'date_joined', 'is_active', )
    ordering = ('-date_joined', )
    list_filter = ('coursemember__course', 'department', 'is_active',)
    filter_horizontal = ()
    inlines = [CourseMemberInlineAdmin, ]
    search_fields = ('username', 'first_name', 'last_name', 'nick_name', 'email', 'mobile', 'facebook', )
    export_resource = MemberResource

    def custom_name(self, obj):
        return u'(%s) %s %s' % (obj.nick_name or '-', obj.first_name, obj.last_name)

    custom_name.short_description = u'ชื่อเล่น / ชื่อจริง'
    custom_name.admin_order_field = 'nick_name'

    def custom_course_list(self, obj):
        courses = obj.coursemember_set.all()
        if courses:
            data = [u'<div>{0}</div>'.format(c.course.name) for c in courses]
            return ''.join(data)
        
        return '-'

    custom_course_list.short_description = u'รายชื่อวิชาเรียน'
    custom_course_list.allow_tags = True

    def picture_preview(self, obj):
        if obj.picture:
            return '<img src="{0}" style="width:300px;">'.format(obj.picture.url)

        return '-'

    picture_preview.short_description = 'ตัวอย่างรูปโปรไฟล์'
    picture_preview.allow_tags = True

    def get_export_resource_class(self):
        return self.export_resource

site.register(Member, MemberAdmin)


class RemainingHoursFilter(admin.SimpleListFilter):
    title = u'ชั่วโมงเรียนคงเหลือ'
    parameter_name = 'remaining_studying_hours'

    def lookups(self, request, model_admin):
        return (
            ('<3', u'น้อยกว่า 3 ชั่วโมง'),
            ('<5', u'น้อยกว่า 5 ชั่วโมง'),
            ('5-10', u'ระหว่าง 5 ถึง 10 ชั่วโมง'),
            ('>10', u'มากกว่า 10 ชั่วโมง'),
        )

    def queryset(self, request, queryset):
        if self.value() == '<3':
            return queryset.filter(remaining_studying_hours__lt=3)
        if self.value() == '<5':
            return queryset.filter(remaining_studying_hours__lt=5)
        if self.value() == '5-10':
            return queryset.filter(remaining_studying_hours__gte=5, remaining_studying_hours__lte=10)
        if self.value() == '>10':
            return queryset.filter(remaining_studying_hours__gt=10)

        return queryset


class CourseUsageAdmin(admin.ModelAdmin):
    model = CourseUsage
    ordering = ('-last_updated', )
    actions = None

    list_display = ('get_member', 'course', 'remaining_studying_hours', )
    search_fields = ('member__username', 'member__first_name', 'member__last_name',
                     'member__nick_name', 'member__email', 'member__facebook', )
    list_filter = (RemainingHoursFilter, 'course')

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def get_member(self, obj):
        url = reverse('admin:member_member_change', args=(obj.member.id,))
        return '<a href="{0}">{1}</a>'.format(url, obj.member.get_full_name())

    get_member.short_description = u'นักเรียน'
    get_member.allow_tags = True
    get_member.ordering = 'member'

site.register(CourseUsage, CourseUsageAdmin)
