from __future__ import unicode_literals

from app_settings.models import SettingProfile
from booking.models import (
    BookingPeriod,
    BookingActionEnum,
)
import datetime


def get_current_course(current_datetime, member):
    setting_profile = SettingProfile.get_default()
    early_login_time = datetime.timedelta(
        minutes=setting_profile.sls_early_login_time
    )

    c_date = current_datetime.date()
    c_time = current_datetime.time()
    booked_periods = BookingPeriod.objects.filter(
        booking__date=c_date,
        booking__member=member,
        booking__action=BookingActionEnum.BOOKED,
    ).select_related()

    current_period = None
    for booked_period in booked_periods:
        start_time = booked_period.period.start_time
        end_time = booked_period.period.end_time
        early_time = (datetime.datetime.combine(c_date, start_time) - early_login_time).time()
        if early_time < c_time < end_time:
            current_period = booked_period
            break

    if not current_period:
        return (None, None, )

    current_book = current_period.booking

    end_period = current_book.bookingperiod_set.order_by('-period__end_time').first().period

    return (current_book, end_period, )
