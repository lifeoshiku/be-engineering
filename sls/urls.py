# coding=utf8

from django.conf.urls import url, include
from . import views

urlpatterns = [
    url(r'^$', views.LoginView.as_view(), name='home'),
    url(r'^login$', views.LoginView.as_view(), name='login'),
    url(r'^logout$', views.LogoutView.as_view(), name='logout'),
    url(r'^course$', views.CourseView.as_view(), name='course'),
    url(r'^course/viewer/(?P<media_id>[0-9]+?)$', views.MediaViewerView.as_view(), name='media_viewer'),
]