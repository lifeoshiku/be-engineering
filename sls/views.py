from django.views import View
from django.shortcuts import render, redirect, reverse
from app_settings.models import SettingProfile
from member.models import Member, SLSLoginLog, SLSCourseVideoViewHistory
from frontend.user_views import clear_user_session, set_user_session
from frontend.booking_views import get_member
from course.models import Course, CourseVideo
from booking.models import Booking
from datetime import datetime
import dateutil.parser
from . import utils


class BaseLoggedInView(View):

    _default_context = None
    login_url = 'sls:login'

    def dispatch(self, request, *args, **kwargs):
        if 'id' not in request.session:
            return redirect(reverse(self.login_url))

        self.request.user = get_member(request)
        self.member = self.request.user

        return super().dispatch(request, *args, **kwargs)

    def get_default_context(self):
        logout_datetime = self.request.session['logout_datetime']
        login_countdown = dateutil.parser.parse(logout_datetime) - datetime.now()

        if not self._default_context:
            self._default_context = {
                'user': self.member,
                'login_countdown': login_countdown.seconds,
            }

        return self._default_context
    
    def force_logout(self, request):
        return redirect(reverse('sls:logout'))


class LoginView(View):
    template_name = 'sls/login.html'
    login_complete_url = 'sls:course'

    def dispatch(self, request, *args, **kwargs):
        if 'id' in request.session:
            return redirect(reverse(self.login_complete_url))
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {})

    def post(self, request, *args, **kwargs):
        context = {}

        username = request.POST.get('username', '')
        password = request.POST.get('password', '')

        if username != '' and password != '':
            try:
                member = Member.objects.get(username=username)
                if not member.is_active:
                    context['user_inactive'] = True
                elif member.check_password(password):
                    (booking, last_period, ) = utils.get_current_course(datetime.now(), member)

                    if not booking:
                        context['no_book'] = True
                    else:
                        set_user_session(request, member)
                        request.session['book_id'] = booking.id
                        logout_datetime = datetime.combine(datetime.today(), last_period.end_time)
                        request.session['logout_datetime'] = logout_datetime.isoformat()
                        
                        login_log = SLSLoginLog(booking=booking)
                        login_log.save()

                        url = reverse(self.login_complete_url)
                        return redirect(url)
                else:
                    context['invalid_password'] = True
            except Member.DoesNotExist:
                context['user_not_found'] = True

            context['username'] = username

        return render(request, self.template_name, context)


class LogoutView(BaseLoggedInView):

    after_logout_url = 'sls:login'

    def get(self, request, *args, **kwargs):
        clear_user_session(request)
        if 'book_id' in request.session:
            del request.session['book_id']
        if 'logout_datetime' in request.session:
            del request.session['logout_datetime']

        url = reverse(self.after_logout_url)
        return redirect(url)


class CourseView(BaseLoggedInView):

    template_name = 'sls/course.html'

    def get(self, request, *args, **kwargs):
        context = self.get_default_context()

        book_id = request.session.get('book_id', None)

        if not book_id:
            return self.force_logout(request)

        try:
            booking = Booking.objects.get(id=book_id)
        except Booking.DoesNotExist:
            booking = None

        if not booking:
            return self.force_logout(request)

        current_course = booking.course
        media_list = current_course.coursevideo_set.filter(public=True)
        view_history_list = SLSCourseVideoViewHistory.objects.filter(
            course_video__course=current_course
        ).values_list('course_video__id', flat=True)

        context = self.get_default_context()
        context.update({
            'video_list': media_list,
            'course': current_course,
            'history_list': view_history_list,
        })

        return render(request, self.template_name, context)


class MediaViewerView(BaseLoggedInView):

    template_name = 'sls/media_viewer.html'

    def get(self, request, media_id, *args, **kwargs):
        context = self.get_default_context()
        try:
            video = CourseVideo.objects.get(id=media_id)
        except CourseVideo.DoesNotExist:
            return redirect(reverse('sls:course'))

        view_log = SLSCourseVideoViewHistory(
            member=self.member,
            course_video=video
        )
        view_log.save()

        settings = SettingProfile.get_default()

        context.update({
            'video': video,
            'media_server_url': settings.sls_media_server
        })
        return render(request, self.template_name, context)
