"""BeEngineer URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from . import settings
from django.conf.urls import url, include
from custom_admin.sites import site
from django.conf.urls.static import static
from member.views import MemberAutocomplete, CourseMemberAutocomplete


urlpatterns = [
    url(r'^autocomplete/member$', MemberAutocomplete.as_view(), name='member-autocomplete',),
    url(r'^autocomplete/course-member$', CourseMemberAutocomplete.as_view(), name='course-member-autocomplete',),
    url(r'^admin/', site.urls),
    url(r'^sls-client/', include('sls.urls', namespace='sls')),
    url(r'^', include('frontend.urls', namespace='frontend')),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
