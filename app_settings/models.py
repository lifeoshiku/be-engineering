# coding=utf8
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.db.models import *
from django.db import models as models
from polymorphic.models import PolymorphicModel


def limit_menu_choices():
    return {
        'public': True
    }

class SpecialDayOff(models.Model):

    # Fields
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    public = models.BooleanField(null=False, default=True, db_index=True,)
    start_date = models.DateField(null=False, blank=False)
    end_date = models.DateField(null=False, blank=False)


    class Meta:
        ordering = ('-created',)
        verbose_name = u'วันหยุดพิเศษ'
        verbose_name_plural = u'วันหยุดพิเศษ'

    def __str__(self):
        return u'%s - %s' % (self.start_date, self.end_date,)

    def __unicode__(self):
        return u'%s - %s' % (self.start_date, self.end_date,)


class SpecialDayOn(models.Model):

    # Fields
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    public = models.BooleanField(null=False, default=True, db_index=True,)
    start_date = models.DateField(null=False, blank=False)
    end_date = models.DateField(null=False, blank=False)


    class Meta:
        ordering = ('-created',)
        verbose_name = u'วันทำการพิเศษ'
        verbose_name_plural = u'วันทำการพิเศษ'

    def __str__(self):
        return u'%s - %s' % (self.start_date, self.end_date,)

    def __unicode__(self):
        return u'%s - %s' % (self.start_date, self.end_date,)


class SpecialPeriodOfDate(models.Model):

    # Fields
    start_date = models.DateField(db_index=True)
    public = models.BooleanField(null=False, default=False, db_index=True,)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    end_date = models.DateField()


    class Meta:
        ordering = ('-created',)
        verbose_name = u'เวลาเรียนสำหรับวันพิเศษ'
        verbose_name_plural = u'เวลาเรียนสำหรับวันพิเศษ'

    def __str__(self):
        return u'%s - %s' % (self.start_date, self.end_date,)

    def __unicode__(self):
        return u'%s - %s' % (self.start_date, self.end_date,)


class Period(models.Model):

    # Fields
    start_time = models.TimeField(null=False, blank=False)
    end_time = models.TimeField(null=False, blank=False)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    public = models.BooleanField(null=False, default=True, db_index=True,)


    class Meta:
        ordering = ('start_time',)
        verbose_name = u'เวลาเรียน'
        verbose_name_plural = u'เวลาเรียน'

    def __str__(self):
        return u'%s - %s' % (self.start_time.strftime('%H:%M'), self.end_time.strftime('%H:%M'),)

    def __unicode__(self):
        return u'%s - %s' % (self.start_time.strftime('%H:%M'), self.end_time.strftime('%H:%M'),)


class SpecialPeriodOfDateLineItem(models.Model):

    # Fields
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    public = models.BooleanField(null=False, default=True, db_index=True,)

    # Relationship Fields
    special_date = models.ForeignKey('app_settings.SpecialPeriodOfDate', )
    period = models.ForeignKey(
        'app_settings.Period',
        limit_choices_to=limit_menu_choices
    )

    class Meta:
        ordering = ('period',)
        verbose_name = u'เวลาเรียน'
        verbose_name_plural = u'เวลาเรียน'

    def __str__(self):
        return u'%s' % self.period

    def __unicode__(self):
        return u'%s' % self.period


class Seat(models.Model):

    # Fields
    name = models.CharField(max_length=255, unique=True)
    public = models.BooleanField(null=False, default=True, db_index=True,)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)


    class Meta:
        ordering = ('name',)
        verbose_name = u'ที่นั่ง'
        verbose_name_plural = u'ที่นั่ง'

    def __str__(self):
        return self.name

    def __unicode__(self):
        return u'%s' % self.name


class SettingProfilePeriod(PolymorphicModel):

    # Fields
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    public = models.BooleanField(null=False, default=True, db_index=True,)

    # Relationship Fields
    profile = models.ForeignKey('app_settings.SettingProfile', )
    period = models.ForeignKey('app_settings.Period', )

    class Meta:
        ordering = ('period',)
        verbose_name = u'เวลาเรียนปกติ'
        verbose_name_plural = u'เวลาเรียนปกติ'

    def __str__(self):
        return u'%s' % self.period

    def __unicode__(self):
        return u'%s' % self.period


class SettingProfileSundayPeriod(SettingProfilePeriod):

    class Meta:
        ordering = ('period',)
        verbose_name = u'เวลาเรียนปกติ (อาทิตย์)'
        verbose_name_plural = verbose_name


class SettingProfileMondayPeriod(SettingProfilePeriod):

    class Meta:
        ordering = ('period',)
        verbose_name = u'เวลาเรียนปกติ (จันทร์)'
        verbose_name_plural = verbose_name


class SettingProfileTuesdayPeriod(SettingProfilePeriod):

    class Meta:
        ordering = ('period',)
        verbose_name = u'เวลาเรียนปกติ (อังคาร)'
        verbose_name_plural = verbose_name


class SettingProfileWednesdayPeriod(SettingProfilePeriod):

    class Meta:
        ordering = ('period',)
        verbose_name = u'เวลาเรียนปกติ (พุธ)'
        verbose_name_plural = verbose_name


class SettingProfileThursdayPeriod(SettingProfilePeriod):

    class Meta:
        ordering = ('period',)
        verbose_name = u'เวลาเรียนปกติ (พฤหัส)'
        verbose_name_plural = verbose_name


class SettingProfileFridayPeriod(SettingProfilePeriod):

    class Meta:
        ordering = ('period',)
        verbose_name = u'เวลาเรียนปกติ (ศุกร์)'
        verbose_name_plural = verbose_name


class SettingProfileSaturdayPeriod(SettingProfilePeriod):

    class Meta:
        ordering = ('period',)
        verbose_name = u'เวลาเรียนปกติ (เสาร์)'
        verbose_name_plural = verbose_name


WEEKDAY_PERIOD_CLASS = [
    SettingProfileMondayPeriod,
    SettingProfileTuesdayPeriod,
    SettingProfileWednesdayPeriod,
    SettingProfileThursdayPeriod,
    SettingProfileFridayPeriod,
    SettingProfileSaturdayPeriod,
    SettingProfileSundayPeriod,
]


class SettingProfile(models.Model):

    # Fields
    name = models.CharField(max_length=255, unique=True)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    sunday = models.BooleanField(
        default=False,
        null=False,
        verbose_name=u'เปิดใช้งานวันอาทิตย์',
    )
    monday = models.BooleanField(
        default=True,
        null=False,
        verbose_name=u'เปิดใช้งานวันจันทร์',
    )
    tuesday = models.BooleanField(
        default=True,
        null=False,
        verbose_name=u'เปิดใช้งานวันอังคาร',
    )
    wednesday = models.BooleanField(
        default=True,
        null=False,
        verbose_name=u'เปิดใช้งานวันพุธ',
    )
    thursday = models.BooleanField(
        default=True,
        null=False,
        verbose_name=u'เปิดใช้งานวันพฤหัส',
    )
    friday = models.BooleanField(
        default=True,
        null=False,
        verbose_name=u'เปิดใช้งานวันศุกร์',
    )
    saturday = models.BooleanField(
        default=False,
        null=False,
        verbose_name=u'เปิดใช้งานวันเสาร์',
    )
    minute_before_cancel = models.PositiveIntegerField(
        default=6,
        null=False,
    )
    late_booking_minute = models.PositiveIntegerField(
        default=45,
        null=False,
    )
    sls_media_server = models.CharField(
        max_length=255,
        null=False,
        default='file://nas.server',
        verbose_name=u'Media Server URL',
        help_text=u'เช่น http://10.0.0.1 file:///User/home/files (*ห้ามมี Slash ต่อท้าย)'
    )
    sls_clear_booking_status_minute = models.PositiveIntegerField(
        default=15,
        null=False,
        verbose_name=u'(SLS) ระยะเวลาที่อนุญาตให้มาสาย (นาที)'
    )
    sls_early_login_time = models.PositiveIntegerField(
        default=0,
        null=False,
        help_text=u'(SLS) เวลาที่สามารถให้ login ล่วงหน้าได้ หน่วยเป็น นาที'
    )
    default = models.BooleanField(
        null=False,
        default=True,
        db_index=True,
        verbose_name=u'ใช้การตั้งค่านี้',
    )

    class Meta:
        ordering = ('-created',)
        verbose_name = u'ตั้งค่าทั่วไป'
        verbose_name_plural = u'ตั้งค่าทั่วไป'

    def __str__(self):
        return u'%s' % self.name

    def __unicode__(self):
        return u'%s' % self.name

    def get_weekday_status(self):
        return [
            self.monday,
            self.tuesday,
            self.wednesday,
            self.thursday,
            self.friday,
            self.saturday,
            self.sunday,
        ]

    def get_periods(self, date_obj):
        weekday = date_obj.weekday()
        # python start day of week is Monday
        weekday_status = self.get_weekday_status()

        if weekday_status[weekday]:
            period_class = WEEKDAY_PERIOD_CLASS[weekday]
            return period_class.objects.filter(profile=self, public=True)

        return []

    def save(self, *args, **kwargs):
        if self.default is True:
            self.__class__.objects.all().update(default=False)

        super(self.__class__, self).save(*args, **kwargs)

    @staticmethod
    def get_default():
        try:
            default = SettingProfile.objects.get(default=True)
        except SettingProfile.DoesNotExist:
            raise Exception('profile setting must be only 1 public profile')

        return default


class Department(models.Model):

    name = models.CharField(max_length=255, unique=True)
    public = models.BooleanField(null=False, default=True, db_index=True,)
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        ordering = ('-name',)
        verbose_name = u'คณะ/ภาควิชา'
        verbose_name_plural = verbose_name

    def __str__(self):
        return u'%s' % self.name

    def __unicode__(self):
        return u'%s' % self.name