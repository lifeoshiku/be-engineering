from django.test import TestCase
from .models import (
    Period,
    SettingProfile,
    SettingProfileSundayPeriod,
    SettingProfileMondayPeriod,
    SettingProfileTuesdayPeriod,
    SettingProfileWednesdayPeriod,
    SettingProfileThursdayPeriod,
    SettingProfileFridayPeriod,
    SettingProfileSaturdayPeriod,
)
import datetime

SUNDAY_PERIODS_COUNT = 10
MONDAY_PERIODS_COUNT = 11
TUESDAY_PERIODS_COUNT = 12
WEDNESDAY_PERIODS_COUNT = 13
THURSDAY_PERIODS_COUNT = 14
FRIDAY_PERIODS_COUNT = 15
SATURDAY_PERIODS_COUNT = 16

BEGIN_OF_PERIOD = 3


def create_period(profile, periods_count, day_class):
    periods = Period.objects.filter(public=True)
    for p in periods[BEGIN_OF_PERIOD: BEGIN_OF_PERIOD + periods_count]:
        day_class(
            profile=profile,
            period=p,
            public=True,
        ).save()


def create_default_settings():
    profile = SettingProfile(
        name='default setting',
        sunday=True,
        monday=True,
        tuesday=True,
        wednesday=True,
        thursday=True,
        friday=True,
        saturday=True,
        default=True,
    )
    profile.save()

    create_period(profile, SUNDAY_PERIODS_COUNT, SettingProfileSundayPeriod)
    create_period(profile, MONDAY_PERIODS_COUNT, SettingProfileMondayPeriod)
    create_period(profile, TUESDAY_PERIODS_COUNT, SettingProfileTuesdayPeriod)
    create_period(profile, WEDNESDAY_PERIODS_COUNT, SettingProfileWednesdayPeriod)
    create_period(profile, THURSDAY_PERIODS_COUNT, SettingProfileThursdayPeriod)
    create_period(profile, FRIDAY_PERIODS_COUNT, SettingProfileFridayPeriod)
    create_period(profile, SATURDAY_PERIODS_COUNT, SettingProfileSaturdayPeriod)



class AppSettingsTestCase(TestCase):

    def setUp(self):
        create_default_settings()

    def test_0001_period_list_of_each_day_should_equal_to_database(self):
        sunday = datetime.date(2018, 7, 1)
        monday = datetime.date(2018, 7, 2)
        tuesday = datetime.date(2018, 7, 3)
        wednesday = datetime.date(2018, 7, 4)
        thursday = datetime.date(2018, 7, 5)
        friday = datetime.date(2018, 7, 6)
        saturday = datetime.date(2018, 7, 7)

        setting = SettingProfile.get_default()

        data = (
            (sunday, SettingProfileSundayPeriod),
            (monday, SettingProfileMondayPeriod),
            (tuesday, SettingProfileTuesdayPeriod),
            (wednesday, SettingProfileWednesdayPeriod),
            (thursday, SettingProfileThursdayPeriod),
            (friday, SettingProfileFridayPeriod),
            (saturday, SettingProfileSaturdayPeriod),
        )

        for (date, period_class) in data:
            periods = setting.get_periods(date)
            expected_periods = period_class.objects.filter(profile=setting, public=True)
            expected_count = expected_periods.count()
            self.assertEqual(len(periods), expected_count)

    def test_0002_period_list_in_not_default_day_should_be_empty_list(self):
        setting = SettingProfile.get_default()
        setting.sunday = False
        setting.save()

        sunday = datetime.date(2018, 7, 1)
        self.assertEqual(setting.get_periods(sunday), [])
