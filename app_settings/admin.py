from django.contrib import admin
from django import forms
from .models import (
    SpecialDayOff,
    SpecialDayOn,
    SpecialPeriodOfDate,
    Period,
    SpecialPeriodOfDateLineItem,
    Seat,
    SettingProfile,
    SettingProfilePeriod,
    Department,
    SettingProfileSundayPeriod,
    SettingProfileMondayPeriod,
    SettingProfileTuesdayPeriod,
    SettingProfileWednesdayPeriod,
    SettingProfileThursdayPeriod,
    SettingProfileFridayPeriod,
    SettingProfileSaturdayPeriod,
)
from custom_admin.sites import site


class SpecialDayOffAdminForm(forms.ModelForm):

    class Meta:
        model = SpecialDayOff
        fields = '__all__'


class SpecialDayOffAdmin(admin.ModelAdmin):
    form = SpecialDayOffAdminForm
    list_display = ['start_date', 'end_date', 'public', 'created', 'last_updated',]
    readonly_fields = ['created', 'last_updated']

site.register(SpecialDayOff, SpecialDayOffAdmin)


class SpecialDayOnAdminForm(forms.ModelForm):

    class Meta:
        model = SpecialDayOn
        fields = '__all__'


class SpecialDayOnAdmin(admin.ModelAdmin):
    form = SpecialDayOnAdminForm
    list_display = ['start_date', 'end_date', 'public', 'created', 'last_updated',]
    readonly_fields = ['created', 'last_updated']

site.register(SpecialDayOn, SpecialDayOnAdmin)


class PeriodAdminForm(forms.ModelForm):

    class Meta:
        model = Period
        fields = '__all__'


class PeriodAdmin(admin.ModelAdmin):
    form = PeriodAdminForm
    list_display = ['display_period', 'created', 'last_updated', 'public']
    readonly_fields = ['created', 'last_updated']

    def display_period(self, obj):
        return u'%s - %s' % (obj.start_time, obj.end_time,)

site.register(Period, PeriodAdmin)


class SpecialPeriodOfDateLineItemAdminForm(forms.ModelForm):

    class Meta:
        model = SpecialPeriodOfDateLineItem
        fields = '__all__'


class SpecialPeriodOfDateLineItemInlineAdmin(admin.TabularInline):
    model = SpecialPeriodOfDateLineItem
    list_display = ['created', 'last_updated', 'public']
    readonly_fields = ['created', 'last_updated', 'public']
    extra = 1


class SpecialPeriodOfDateAdminForm(forms.ModelForm):

    class Meta:
        model = SpecialPeriodOfDate
        fields = '__all__'


class SpecialPeriodOfDateAdmin(admin.ModelAdmin):
    form = SpecialPeriodOfDateAdminForm
    list_display = ['start_date', 'end_date', 'public', 'created',
                    'last_updated', ]
    readonly_fields = ['created', 'last_updated', ]
    inlines = [SpecialPeriodOfDateLineItemInlineAdmin, ]

site.register(SpecialPeriodOfDate, SpecialPeriodOfDateAdmin)


class SeatAdminForm(forms.ModelForm):

    class Meta:
        model = Seat
        fields = '__all__'


class SeatAdmin(admin.ModelAdmin):
    form = SeatAdminForm
    list_display = ['name', 'public', 'created', 'last_updated']
    readonly_fields = ['created', 'last_updated']

site.register(Seat, SeatAdmin)


class SettingProfilePeriodInlineAdmin(admin.TabularInline):
    model = SettingProfilePeriod
    list_display = ['public', 'created', 'last_updated', ]
    readonly_fields = ['created', 'last_updated']
    extra = 1

class SettingProfileSundayPeriodInlineAdmin(SettingProfilePeriodInlineAdmin):
    model = SettingProfileSundayPeriod


class SettingProfileMondayPeriodInlineAdmin(SettingProfilePeriodInlineAdmin):
    model = SettingProfileMondayPeriod


class SettingProfileTuesdayPeriodInlineAdmin(SettingProfilePeriodInlineAdmin):
    model = SettingProfileTuesdayPeriod


class SettingProfileWednesdayPeriodInlineAdmin(SettingProfilePeriodInlineAdmin):
    model = SettingProfileWednesdayPeriod


class SettingProfileThursdayPeriodInlineAdmin(SettingProfilePeriodInlineAdmin):
    model = SettingProfileThursdayPeriod


class SettingProfileFridayPeriodInlineAdmin(SettingProfilePeriodInlineAdmin):
    model = SettingProfileFridayPeriod


class SettingProfileSaturdayPeriodInlineAdmin(SettingProfilePeriodInlineAdmin):
    model = SettingProfileSaturdayPeriod



class SettingProfileAdminForm(forms.ModelForm):

    class Meta:
        model = SettingProfile
        fields = '__all__'


class SettingProfileAdmin(admin.ModelAdmin):
    form = SettingProfileAdminForm
    list_display = ['name', 'default', 'sunday', 'monday', 'tuesday', 
                    'wednesday', 'thursday', 'friday', 'saturday',
                    'minute_before_cancel', 'created', 'last_updated', ]
    readonly_fields = ['created', 'last_updated', ]
    inlines = [
        SettingProfileSundayPeriodInlineAdmin,
        SettingProfileMondayPeriodInlineAdmin,
        SettingProfileTuesdayPeriodInlineAdmin,
        SettingProfileWednesdayPeriodInlineAdmin,
        SettingProfileThursdayPeriodInlineAdmin,
        SettingProfileFridayPeriodInlineAdmin,
        SettingProfileSaturdayPeriodInlineAdmin,
    ]

site.register(SettingProfile, SettingProfileAdmin)


class DepartmentAdmin(admin.ModelAdmin):
    list_display = ['name', 'public', 'created', 'last_updated', ]
    readonly_fields = ['created', 'last_updated', ]

site.register(Department, DepartmentAdmin)
