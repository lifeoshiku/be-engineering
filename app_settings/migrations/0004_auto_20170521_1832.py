# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-21 11:32
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_settings', '0003_auto_20170521_0926'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='period',
            options={'ordering': ('-start_time',), 'verbose_name': 'เวลาเรียน', 'verbose_name_plural': 'เวลาเรียน'},
        ),
        migrations.AlterModelOptions(
            name='seat',
            options={'ordering': ('-created',), 'verbose_name': 'ที่นั่ง', 'verbose_name_plural': 'ที่นั่ง'},
        ),
        migrations.AlterModelOptions(
            name='settingprofile',
            options={'ordering': ('-created',), 'verbose_name': 'ตั้งค่าทั่วไป', 'verbose_name_plural': 'ตั้งค่าทั่วไป'},
        ),
        migrations.AlterModelOptions(
            name='settingprofileperiod',
            options={'ordering': ('-created',), 'verbose_name': 'เวลาเรียนปกติ', 'verbose_name_plural': 'เวลาเรียนปกติ'},
        ),
        migrations.AlterModelOptions(
            name='specialdayoff',
            options={'ordering': ('-created',), 'verbose_name': 'วันหยุดพิเศษ', 'verbose_name_plural': 'วันหยุดพิเศษ'},
        ),
        migrations.AlterModelOptions(
            name='specialdayon',
            options={'ordering': ('-created',), 'verbose_name': 'วันทำการพิเศษ', 'verbose_name_plural': 'วันทำการพิเศษ'},
        ),
        migrations.AlterModelOptions(
            name='specialperiodofdate',
            options={'ordering': ('-created',), 'verbose_name': 'เวลาเรียนสำหรับวันพิเศษ', 'verbose_name_plural': 'เวลาเรียนสำหรับวันพิเศษ'},
        ),
        migrations.AlterModelOptions(
            name='specialperiodofdatelineitem',
            options={'ordering': ('-created',), 'verbose_name': 'เวลาเรียน', 'verbose_name_plural': 'เวลาเรียน'},
        ),
    ]
