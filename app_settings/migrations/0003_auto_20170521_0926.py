# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-05-21 09:26
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app_settings', '0002_auto_20170521_0833'),
    ]

    operations = [
        migrations.RenameField(
            model_name='settingprofile',
            old_name='hour_before_cancel',
            new_name='minute_before_cancel',
        ),
    ]
