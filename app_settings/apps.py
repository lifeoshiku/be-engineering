# coding=utf8
from django.apps import AppConfig


class SettingsConfig(AppConfig):
    name = 'app_settings'
    verbose_name = u'ตั้งค่าและปรับแต่ง'

