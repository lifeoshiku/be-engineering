# coding=utf8
from __future__ import unicode_literals

from django.db import models
import os
from datetime import datetime
import time
from versatileimagefield.fields import VersatileImageField
from adminsortable.models import SortableMixin


def make_upload_filename(filename):
    (name, ext,) = os.path.splitext(filename)
    timestamp = time.mktime(datetime.today().timetuple())
    new_filename = name + '_' + str(timestamp) + ext
    return new_filename


def update_filename(instance, filename):
    path = os.path.join('images/slider/', instance.slide.name)
    format = make_upload_filename(filename)
    return os.path.join(path, format)


class Slider(models.Model):
    name = models.CharField(
        max_length=32,
        null=False,
        blank=False,
    )
    hint = models.CharField(
        max_length=300,
        null=False,
        blank=True,
        default='',
    )
    update_at = models.DateTimeField(
        auto_now_add=False,
        auto_now=True,
        null=True
    )
    create_at = models.DateTimeField(
        auto_now_add=True,
        auto_now=False,
        null=True
    )
    default = models.BooleanField(
        null=False,
        default=True,
        db_index=True,
        verbose_name=u'ใช้การตั้งค่านี้',
    )

    def save(self, *args, **kwargs):
        if self.default is True:
            self.__class__.objects.all().update(default=False)

        super(self.__class__, self).save(*args, **kwargs)

    @staticmethod
    def get_default():
        try:
            default = Slider.objects.get(default=True)
        except Slider.DoesNotExist:
            return None

        return default

    def __unicode__(self):
        return u'%s' % self.name
    
    class Meta:
        verbose_name = "Slider"
        verbose_name_plural = "Sliders"


class SliderItem(SortableMixin):
    slide = models.ForeignKey(
        Slider,
        null=True
    )
    image = VersatileImageField(
        upload_to=update_filename,
        blank=False,
        null=True,
        verbose_name=u'รูปภาพ (ต้องมีขนาด 1130px x 250px เท่านั้น)',
        help_text='image size should be 1130px x 250px',
    )
    position = models.PositiveIntegerField(
        default=0,
        db_index=True,
        editable=False,
    )
    hidden = models.BooleanField(default=False)

    def __str__(self):
        return u'%s' % self.image

    def __unicode__(self):
        return u'%s' % self.image

    class Meta:
        ordering = ('position', )
        verbose_name = "Slider Item"
        verbose_name_plural = "Slider Items"
