# encoding=utf8
from __future__ import unicode_literals

from .models import Slider, SliderItem
from django.db import models
from django.forms.widgets import ClearableFileInput
from adminsortable.admin import SortableTabularInline, NonSortableParentAdmin
from custom_admin.sites import site
from versatileimagefield.fields import VersatileImageField


class ImagePreviewInput(ClearableFileInput):
    template_name = 'admin/image_preview_widget.html'


class SliderItemInlineAdmin(SortableTabularInline):
    model = SliderItem
    extra = 1
    fields = ('image', 'hidden', )

    formfield_overrides = {
        VersatileImageField: {'widget': ImagePreviewInput},
    }


class SliderAdmin(NonSortableParentAdmin):
    list_display = ('name', 'hint', 'slider_count', 'default', 'update_at')
    search_fields = ['name', ]
    readonly_fields = ('update_at', 'create_at', )
    fieldsets = [
        ("Slide", {'fields': [
            'name',
            'hint',
            'default',
            ('create_at', 'update_at', )
        ]})
    ]

    actions = []

    def slider_count(self, instance):
        return instance.slideritem_set.all().count()

    inlines = [SliderItemInlineAdmin, ]

site.register(Slider, SliderAdmin)
