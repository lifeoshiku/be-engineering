# coding=utf8

from .decorators import login_required, login_not_required
from .utils import get_member, get_member_id
from member.models import Member
from app_settings.models import Department
from django.shortcuts import render, redirect, reverse
from django.contrib.auth.password_validation import validate_password
from django.core.exceptions import ValidationError
from django.forms.models import model_to_dict
from frontend.booking_views import get_default_context


DEFAULT_DASHBOARD_URL = 'frontend:search_booking'


def set_user_session(request, member):
    request.session['id'] = member.id
    request.session['username'] = member.id
    request.session['first_name'] = member.first_name
    request.session['last_name'] = member.last_name
    request.session['email'] = member.email
    request.session['picture_url'] = member.picture.url if member.picture else None


def clear_user_session(request):
    del request.session['id']
    del request.session['username']
    del request.session['first_name']
    del request.session['last_name']
    del request.session['email']


@login_not_required
def login(request):
    context = {}

    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')

        if username != '' and password != '':
            try:
                member = Member.objects.get(username=username)
                if not member.is_active:
                    context['user_inactive'] = True
                elif member.check_password(password):
                    set_user_session(request, member)
                    url = reverse(DEFAULT_DASHBOARD_URL)
                    return redirect(url)
                else:
                    context['invalid_password'] = True
            except Member.DoesNotExist:
                context['user_not_found'] = True

            context['username'] = username

    return render(request, 'frontend/login.html', context)

@login_required
def logout(request):
    clear_user_session(request)
    url = reverse('frontend:login')
    return redirect(url)

@login_not_required
def register(request):
    context = {}
    context['predefined_department'] = Department.objects.filter(public=True)

    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password')
        password2 = request.POST.get('password2')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        gender = request.POST.get('gender')
        nick_name = request.POST.get('nick_name')
        email = request.POST.get('email')
        mobile = request.POST.get('mobile')
        facebook = request.POST.get('facebook')
        department = request.POST.get('department')
        address = request.POST.get('address')
        gpax = request.POST.get('gpax')
        known_form = request.POST.getlist('known_form')
        known_form_other = request.POST.get('known_form_other')

        if '' in [first_name, last_name, email, mobile, facebook, username, ]:
            context.update({
                'known_form': known_form,
                'origin': request.POST,
                'error_fields': [
                    'first_name',
                    'last_name',
                    'email',
                    'mobile',
                    'facebook',
                    'username',
                ],
                'error_message': u'กรุณากรอกข้อมูลให้ครบถ้วน',
            })
            return render(request, 'frontend/register.html', context)

        if len(username) <= 4:
            context.update({
                'known_form': known_form,
                'origin': request.POST,
                'error_fields': [
                    'username',
                ],
                'error_message': u'user ต้องมี 4 ตัวอักษรหรือมากกว่า',
            })
            return render(request, 'frontend/register.html', context)

        if Member.objects.filter(username=username).exists():
            context.update({
                'known_form': known_form,
                'origin': request.POST,
                'error_fields': [
                    'username',
                ],
                'error_message': u'มีผู้ใช้งานนี้ในระบบแล้ว',
            })
            return render(request, 'frontend/register.html', context)

        if password != password2:
            context.update({
                'known_form': known_form,
                'origin': request.POST,
                'error_fields': [
                    'password',
                ],
                'error_message': u'รหัสไม่ตรงกัน',
            })
            return render(request, 'frontend/register.html', context)

        try:
            validate_password(password)
        except ValidationError as e:
            context.update({
                'known_form': known_form,
                'origin': request.POST,
                'error_fields': [
                    'password',
                ],
                'error_message': u'password ต้องมี 8 อักขระหรือมากกว่า, ตัวอักษร, ตัวเลขหรืออักขระพิเศษ',
            })
            return render(request, 'frontend/register.html', context)

        if not request.FILES:
            context.update({
                'known_form': known_form,
                'origin': request.POST,
                'error_fields': [
                    'profile_picture',
                ],
                'error_message': u'กรุณาใส่รูปภาพ',
            })
            return render(request, 'frontend/register.html', context)

        try:
            know_form_raw = known_form
            if known_form_other != '':
                know_form_raw.append(known_form_other)

            member = Member(
                username=username,
                first_name=first_name,
                last_name=last_name,
                gender=gender,
                nick_name=nick_name,
                email=email,
                mobile=mobile,
                facebook=facebook,
                gpax=gpax,
                known_form=', '.join(know_form_raw),
                address=address,
                is_active=False,
            )
            if department != '':
                member.department_id = int(department)
            
            member.set_password(password)
            member.save()

            if request.FILES:
                picture = request.FILES.get('profile_picture')
                member.picture.save(picture.name, picture, save=True)

        except Exception as e:
            context.update({
                'known_form': known_form,
                'origin': request.POST,
                'error_message': u'พบข้อผิดพลาดระหว่างการดำเนินการ',
            })
            print(e)
            return render(request, 'frontend/register.html', context)

        url = reverse('frontend:login')
        return redirect(url)

    return render(request, 'frontend/register.html', context)


@login_required
def edit_profile(request):
    context = get_default_context(request)
    context['predefined_department'] = Department.objects.filter(public=True)

    member = get_member(request)
    context['member'] = member

    if request.POST:
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        gender = request.POST.get('gender')
        nick_name = request.POST.get('nick_name')
        mobile = request.POST.get('mobile')
        facebook = request.POST.get('facebook')
        department = request.POST.get('department')
        address = request.POST.get('address')
        gpax = request.POST.get('gpax')

        if '' in [first_name, last_name, mobile, facebook, ]:
            context.update({
                'origin': request.POST,
                'error_fields': [
                    'first_name',
                    'last_name',
                    'mobile',
                    'facebook',
                ],
                'error_message': u'กรุณากรอกข้อมูลให้ครบถ้วน',
            })
            return render(request, 'frontend/edit_profile.html', context)

        try:
            member.first_name = first_name
            member.last_name = last_name
            member.gender = gender
            member.nick_name = nick_name
            member.mobile = mobile
            member.facebook = facebook
            member.gpax = gpax
            member.address = address
            member.department_id = int(department) if department != '' else None
            member.save()

            if request.FILES:
                picture = request.FILES.get('profile_picture')
                member.picture.save(picture.name, picture, save=True)

            set_user_session(request, member)
        except Exception as e:
            context.update({
                'origin': request.POST,
                'error_message': u'พบข้อผิดพลาดระหว่างการดำเนินการ',
            })
            print(e)
            return render(request, 'frontend/edit_profile.html', context)

        url = reverse(DEFAULT_DASHBOARD_URL)
        return redirect(url)
    else:
        origin = model_to_dict(member)
        origin['department'] = member.department_id or ''
        context['origin'] = origin
        context['known_form'] = member.known_form.split(', ')

    return render(request, 'frontend/edit_profile.html', context)


@login_required
def change_password(request):
    context = get_default_context(request)

    if request.POST:
        old_password = request.POST.get('old_password')
        new_password = request.POST.get('new_password')
        new_password2 = request.POST.get('new_password2')

        member = get_member(request)
        if not member.check_password(old_password):
            context.update({
                'error_fields': [
                    'old_password',
                ],
                'error_message': u'password เดิมไม่ถูกต้อง',
            })
            return render(request, 'frontend/change_password.html', context)

        if new_password != new_password2:
            context.update({
                'error_fields': [
                    'new_password',
                ],
                'error_message': u'รหัสใหม่ไม่ตรงกัน',
            })
            return render(request, 'frontend/change_password.html', context)

        try:
            validate_password(new_password)
        except ValidationError as e:
            context.update({
                'error_fields': [
                    'new_password',
                ],
                'error_message': u'password ต้องมี 8 อักขระหรือมากกว่า, ตัวอักษร, ตัวเลขหรืออักขระพิเศษ',
            })
            return render(request, 'frontend/change_password.html', context)

        try:
            member.set_password(new_password)
            member.save()
        except Exception as e:
            context.update({
                'error_message': u'พบข้อผิดพลาดระหว่างการดำเนินการ',
            })
            print(e)
            return render(request, 'frontend/change_password.html', context)

        url = reverse(DEFAULT_DASHBOARD_URL)
        return redirect(url)

    return render(request, 'frontend/change_password.html', context)