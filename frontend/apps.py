# coding=utf8

from django.apps import AppConfig


class FrontendConfig(AppConfig):
    name = 'frontend'
