# coding=utf8

from django.conf.urls import url, include
from . import user_views, booking_views

urlpatterns = [
    # user
    url(r'^$', user_views.login, name='home_login'),
    url(r'^register$', user_views.register, name='register'),
    url(r'^edit_profile$', user_views.edit_profile, name='edit_profile'),
    url(r'^change_password$', user_views.change_password, name='change_password'),
    url(r'^login$', user_views.login, name='login'),
    url(r'^logout$', user_views.logout, name='logout'),

    # search booking
    url(r'^booking/search$', booking_views.search_booking, 
        name='search_booking'),
    url(r'^booking/book$', booking_views.book, name='book'),
    url(r'^booking/is_free_cancel$',
        booking_views.check_is_free_cancel_booking,
        name='check_is_free_cancel_booking'),
    url(r'^booking/cancel$', booking_views.cancel_booking,
        name='cancel_booking'),

    # booking history
    url(r'^booking/history$', booking_views.history, name='booking_history'),

    # booking schedule
    url(r'^booking/schedule$', booking_views.schedule, name='booking_schedule'),
]