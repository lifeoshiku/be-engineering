# coding=utf8
from .decorators import login_required
from booking.logic import (
    search_booking_slot,
    book_slot,
    is_free_cancel_booking,
    cancel_booking_slot,
)
from booking.exceptions import (
    AlreadyBooked,
    SlotNotAvailable,
    NotEnoughStudyTime,
)
from booking.models import Booking, BookingActionEnum
from course.models import Course
from member.models import CourseMember, Member
from app_settings.models import Period, SettingProfile
from slider.models import Slider
from django.shortcuts import redirect, reverse, render
from django.http import JsonResponse
import datetime
from .utils import get_member, get_member_id


def get_default_context(request):
    context = {}

    member_id = get_member_id(request)
    course_member = CourseMember.objects.filter(member_id=member_id)

    slider = Slider.get_default()
    if slider:
        context['slider'] = slider.slideritem_set.filter(hidden=False)

    context['course_member'] = course_member
    return context


@login_required
def search_booking(request):
    context = get_default_context(request)
    context.update({
        'selected_menu': 'search',
        'search_params': {}
    })

    member = get_member(request)
    course_members = CourseMember.objects.filter(
        member=member,
    )

    context['course_members'] = course_members

    if request.POST:
        course_id = request.POST.get('course', None)
        duration = request.POST.get('duration', None)
        date_str = request.POST.get('date', None)
        addition_date_str = request.POST.get('addition_date', None)

        if not course_id or not duration or not date_str:
            url = reverse('frontend:search_booking')
            return redirect(url)

        course_id = int(course_id)
        try:
            course = Course.objects.get(id=course_id)
        except Course.DoesNotExist:
            raise Exception('course id "%s" is not found' % course_id)

        duration = int(duration)
        duration_time = datetime.timedelta(hours=duration)
        if addition_date_str and addition_date_str != '':
            date = datetime.datetime.strptime(addition_date_str, "%Y-%m-%d").date()
        else:
            date = datetime.datetime.strptime(date_str, "%Y-%m-%d").date()

        result = search_booking_slot(
            date=date,
            course=course,
            duration=duration_time,
            member=member,
        )
        context['result'] = result
        if not result:
            context['result_not_found'] = True

        search_params = {
            'course': course_id,
            'date': addition_date_str or date_str,
            'duration': duration,
        }

        context['next_date'] = date + datetime.timedelta(days=1)
        context['next_date_str'] = context['next_date'].strftime('%Y-%m-%d')
        if date != datetime.date.today():
            context['prev_date'] = date - datetime.timedelta(days=1)
            context['prev_date_str'] = context['prev_date'].strftime('%Y-%m-%d')

        context['search_params'] = search_params

        display_params = {
            'course_name': course.name,
            'date': date,
        }
        context['display_params'] = display_params
    else:
        context['next_date'] = datetime.date.today() + datetime.timedelta(days=1)
        context['next_date_str'] = context['next_date'].strftime('%Y-%m-%d')

    if 'error_message' in request.session:
        context['error_message'] = request.session.get('error_message', None)
        del request.session['error_message']

    return render(request, 'frontend/booking.html', context)


@login_required
def book(request):
    if request.POST:
        period_id_list_str = request.POST.get('period_id_list', None)
        date_str = request.POST.get('date', None)
        course_id = request.POST.get('course', None)

        if period_id_list_str and date_str and course_id:
            date = datetime.datetime.strptime(date_str, "%Y-%m-%d").date()

            course_id = int(course_id)
            try:
                course = Course.objects.get(id=course_id)
            except Course.DoesNotExist:
                raise Exception('course id "%s" is not found' % course_id)

            period_id_list = [int(pid) for pid in period_id_list_str.split(',')]
            period_list = [Period.objects.get(id=pid) for pid in period_id_list]
            member = get_member(request)

            try:
                booked = book_slot(date, course, period_list, member)
            except AlreadyBooked as e:
                request.session['error_message'] = u'ท่านได้เคยทำการจองวิชา/เวลานี้แล้ว กรุณาจองเวลาใหม่อีกครั้ง'
            except SlotNotAvailable as e:
                request.session['error_message'] = u'ที่นั่งสำหรับช่วงเวลานี้เต็มแล้ว กรุณาจองเวลาใหม่อีกครั้ง'
            except NotEnoughStudyTime as e:
                request.session['error_message'] = u'ไม่สามารถจองได้เนื่องจากเวลาเรียนไม่พอ กรุณาจองเวลาใหม่อีกครั้ง'
            else:
                url = reverse('frontend:search_booking')
                return redirect(url)

    url = reverse('frontend:booking_schedule')
    return redirect(url)


@login_required
def check_is_free_cancel_booking(request):
    if request.POST:
        booking_id = request.POST.get('booking_id')
        if booking_id == '':
            return JsonResponse({
                'status': 'error',
            })

        is_free = is_free_cancel_booking(booking=int(booking_id))

        return JsonResponse({
            'status': 'success',
            'data': {
                'is_free': is_free
            }
        })


@login_required
def cancel_booking(request):
    if request.POST:
        booking_id = request.POST.get('booking_id')
        member = get_member(request)
        cancel_booking_slot(booking=int(booking_id), member=member)

    url = reverse('frontend:booking_schedule')
    return redirect(url)


@login_required
def history(request):
    context = get_default_context(request)
    context.update({
        'selected_menu': 'history'
    })

    member_id = get_member_id(request)
    history_list = Booking.objects.filter(
        member_id=member_id,
    ).prefetch_related('bookingperiod_set').order_by('-created')

    result = []
    now = datetime.datetime.now()
    today = now.date()
    for booking in history_list:
        if booking.action == BookingActionEnum.CANCEL_BOOK:
            result.append(booking)
        else:
            if booking.date < today:
                result.append(booking)
            elif booking.date == today:
                periods = booking.bookingperiod_set.all()
                start_time = periods.first().period.start_time
                if datetime.datetime.combine(today, start_time) < now:
                    result.append(booking)

    context['history_list'] = result
    return render(request, 'frontend/booking_history.html', context)


@login_required
def schedule(request):
    context = get_default_context(request)
    context.update({
        'selected_menu': 'schedule'
    })

    member_id = get_member_id(request)
    book_list = Booking.objects.filter(
        member_id=member_id,
        action=BookingActionEnum.BOOKED
    ).prefetch_related('bookingperiod_set').order_by('date')

    setting_profile = SettingProfile.get_default()
    late_booking = datetime.timedelta(
        minutes=setting_profile.late_booking_minute
    )

    result = []
    now = datetime.datetime.now()
    today = now.date()
    for booking in book_list:
        if booking.date > today:
            result.append(booking)
        elif booking.date == today:
            periods = booking.bookingperiod_set.all()
            start_time = periods.first().period.start_time
            if datetime.datetime.combine(today, start_time) + late_booking >= now:
                result.append(booking)

    context['book_list'] = result
    return render(request, 'frontend/booking_schedule.html', context)
