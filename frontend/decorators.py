# coding=utf8

from django.shortcuts import redirect, reverse


def login_required(function=None, redirect_url=None, redirect_field_name=None):
    def _dec(view_func):
        def _view(request, *args, **kwargs):
            if 'id' in request.session:
                return view_func(request, *args, **kwargs)
            else:
                request.session['redirect_path'] = request.get_full_path()
                url = None
                if redirect_field_name and redirect_field_name in request.REQUEST:
                    url = request.REQUEST[redirect_field_name]
                if not url:
                    url = redirect_url
                if not url:
                    url = reverse('frontend:login')
                return redirect(url)

        _view.__name__ = view_func.__name__
        _view.__dict__ = view_func.__dict__
        _view.__doc__ = view_func.__doc__

        return _view
    if function is None:
        return _dec
    else:
        return _dec(function)


def login_not_required(function=None, home_url=None, redirect_field_name=None):
    def _dec(view_func):
        def _view(request, *args, **kwargs):
            if 'id' not in request.session:
                return view_func(request, *args, **kwargs)
            else:
                url = None
                if redirect_field_name and redirect_field_name in request.REQUEST:
                    url = request.REQUEST[redirect_field_name]
                if not url:
                    url = home_url
                if not url:
                    url = reverse('frontend:booking_schedule')
                return redirect(url)

        _view.__name__ = view_func.__name__
        _view.__dict__ = view_func.__dict__
        _view.__doc__ = view_func.__doc__

        return _view
    if function is None:
        return _dec
    else:
        return _dec(function)