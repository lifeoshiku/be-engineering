from member.models import Member


def get_member_id(request):
    return int(request.session['id'])


def get_member(request):
    id = get_member_id(request)
    return Member.objects.get(id=id)
