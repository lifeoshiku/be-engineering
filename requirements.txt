appdirs==1.4.3
appnope==0.1.0
astroid==1.5.2
decorator==4.0.11
diff-match-patch==20121119
Django==1.11.1
django-admin-sortable==2.1
django-appconf==1.0.2
django-autocomplete-light==3.2.10
django-easy-select2==1.3.4
django-import-export==0.5.1
django-polymorphic==1.3
Django-Select2==5.11.1
django-simple-autocomplete==1.11
django-smart-selects==1.5.2
django-versatileimagefield==1.7.1
et-xmlfile==1.0.1
ipdb==0.10.3
ipython==6.1.0
ipython-genutils==0.2.0
isort==4.2.5
jdcal==1.3
jedi==0.10.2
lazy-object-proxy==1.3.1
mccabe==0.6.1
mysqlclient==1.3.12
odfpy==1.3.5
olefile==0.44
openpyxl==2.4.8
packaging==16.8
pep8==1.7.0
pexpect==4.2.1
pickleshare==0.7.4
Pillow==4.0.0
prompt-toolkit==1.0.14
ptyprocess==0.5.1
Pygments==2.2.0
pylint==1.7.1
pyparsing==2.2.0
python-dateutil==2.6.1
pytz==2017.2
PyYAML==3.12
simplegeneric==0.8.1
six==1.10.0
tablib==0.12.1
traitlets==4.3.2
unicodecsv==0.14.1
wcwidth==0.1.7
wrapt==1.10.10
xlrd==1.1.0
xlwt==1.3.0
