from django import forms
from .models import course


class courseForm(forms.ModelForm):
    class Meta:
        model = course
        fields = ['name', 'detail']


