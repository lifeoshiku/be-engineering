# coding=utf8
from django.apps import AppConfig


class CourseConfig(AppConfig):
    name = 'course'
    verbose_name = u'วิชาเรียน'

