# coding=utf8
from __future__ import unicode_literals

from django.db.models import *
from django.db import models as models
from adminsortable.models import SortableMixin
from django.core.validators import MaxValueValidator


class Course(models.Model):

    # Fields
    name = models.CharField(
        max_length=255,
        verbose_name=u'ชื่อ',
        null=False,
        blank=False,
    )
    detail = models.TextField(
        blank=True,
        default='',
        verbose_name=u'รายละเอียด',
    )
    default_hours = models.PositiveIntegerField(
        default=30,
        null=False,
        blank=False,
        verbose_name=u'ชั่วโมงเรียนเริ่มต้น'
    )
    public = models.BooleanField(
        null=False,
        default=True,
        db_index=True,
    )
    created = models.DateTimeField(
        auto_now_add=True,
        editable=False,
    )
    last_updated = models.DateTimeField(
        auto_now=True,
        editable=False
    )

    class Meta:
        ordering = ('name',)
        verbose_name = u'วิชาเรียน'
        verbose_name_plural = u'วิชาเรียน'

    def __str__(self):
        return u'%s' % self.name

    def __unicode__(self):
        return u'%s' % self.name


class CourseVideo(SortableMixin):
    course = models.ForeignKey(Course)
    name = models.CharField(
        max_length=255,
        verbose_name=u'ชื่อ',
        null=False,
        blank=False,
    )
    link = models.CharField(
        max_length=1024,
        verbose_name=u'Video Link/URL',
        help_text='เช่น ///Users/user/video/sample.mp4, D:/data/video.mp4, http://www.youtube.com/sample.mp4',
        null=False,
        blank=False,
    )
    minute_length = models.PositiveSmallIntegerField(
        default=0,
        blank=False,
        null=False,
        verbose_name=u'ความยาววีดีโอ (นาที)',
        validators=[MaxValueValidator(59)]
    )
    hour_length = models.PositiveSmallIntegerField(
        default=0,
        blank=False,
        null=False,
        verbose_name=u'ความยาววีดีโอ (ชั่วโมง)',
    )
    position = models.PositiveIntegerField(
        default=0,
        db_index=True,
        editable=False,
    )
    public = models.BooleanField(
        null=False,
        default=True,
        db_index=True,
    )
    created = models.DateTimeField(
        auto_now_add=True,
        editable=False,
    )
    last_updated = models.DateTimeField(
        auto_now=True,
        editable=False
    )

    class Meta:
        ordering = ('position',)
        verbose_name = u'วิชาเรียน'
        verbose_name_plural = u'วิชาเรียน'

    def __str__(self):
        return u'%s' % self.name

    def __unicode__(self):
        return u'%s' % self.name
