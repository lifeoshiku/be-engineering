# coding=utf8
from __future__ import unicode_literals

from django.contrib import admin
from django import forms
from .models import Course, CourseVideo
from custom_admin.sites import site
from adminsortable.admin import SortableStackedInline, NonSortableParentAdmin


class CourseVideoInlineAdmin(SortableStackedInline):
    model = CourseVideo
    extra = 1
    fields = ('name', 'link', 'hour_length', 'minute_length', 'public', )


class CourseAdminForm(forms.ModelForm):

    class Meta:
        model = Course
        fields = '__all__'


class CourseAdmin(NonSortableParentAdmin):
    form = CourseAdminForm
    list_display = ['name', 'default_hours', 'public', 'created', 'last_updated', ]
    readonly_fields = ['created', 'last_updated', ]

    inlines = [CourseVideoInlineAdmin, ]


site.register(Course, CourseAdmin)
