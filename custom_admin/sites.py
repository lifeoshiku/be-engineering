from .logic import get_booking_table
from django.contrib.admin.sites import AdminSite
from datetime import date, datetime
from django.conf import settings
from django.contrib.auth.models import Group, User
from django.contrib.auth.admin import UserAdmin

class CustomAdminSite(AdminSite):
    site_title = 'Be-Engineer Admin'
    site_header = 'Be-Engineer Admin'
    index_title = 'Be-Engineer Admin'
    index_template = 'admin/home.html'

    def index(self, request, extra_context=None):
        selected_date = request.POST.get('selected_date', None)
        if selected_date and selected_date != '':
            try:
                selected_date = datetime.strptime(selected_date, '%Y-%m-%d')
                selected_date = selected_date.date()
            except:
                selected_date = date.today()
        else:
            selected_date = date.today()
            
        data = get_booking_table(selected_date)
        booking_table = []

        if data:
            for rid, rows in data.items():
                row = []
                for cid, col in rows.items():
                    row.append(col)
                booking_table.append(row)

        context = dict(
            booking_table=booking_table,
            selected_date=selected_date,
        )
        context.update(extra_context or {})
        return super(self.__class__, self).index(request, context)

site = CustomAdminSite(name='customadmin')

site.register(User, UserAdmin)
# site.register(Group)
