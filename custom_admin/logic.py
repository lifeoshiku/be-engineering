from booking.logic import (
    is_special_day_off,
    is_special_day_on,
    is_default_day,
    get_special_periods,
    get_default_periods
)
from app_settings.models import Seat
from booking.models import BookingPeriod, BookingActionEnum
from collections import OrderedDict
import datetime


def generate_datatable(periods, seats):
    table = OrderedDict()

    for period in periods:
        row = OrderedDict()
        for seat in seats:
            row[seat.id] = {
                'seat': seat,
                'period': period,
            }
        table[period.id] = row

    return table


def get_booking_table(date=None):
    if type(date) != datetime.date:
        raise Exception('date should be datetime.date object')

    if is_special_day_on(date) or is_default_day(date):
        special_periods = get_special_periods(date)
        default_periods = get_default_periods(date)
        periods = special_periods or default_periods

        seats = Seat.objects.filter(public=True)
        table = generate_datatable(periods, seats)

        booking_data = BookingPeriod.objects.filter(
            booking__date=date,
            booking__action=BookingActionEnum.BOOKED
        ).select_related()

        if booking_data:
            for item in booking_data:
                booking = item.booking

                table[item.period.id][booking.seat.id].update({
                    'course': booking.course,
                    'member': booking.member,
                    'booking': booking
                })
            
            row_index_list = list(table.keys())
            col_index_list = list(table[row_index_list[0]].keys())
            for row_index, row_key in enumerate(row_index_list):
                for col_index, col_key in enumerate(col_index_list):
                    is_continue = False
                    current_data = table[row_key][col_key]

                    prev_row_index = row_index - 1
                    if prev_row_index >= 0:
                        prev_row_key = row_index_list[prev_row_index]
                        prev_data = table[prev_row_key][col_key]
                        if 'member' in current_data and 'member' in prev_data:
                            if current_data['member'] == prev_data['member']:
                                is_continue = True

                    next_row_index = row_index + 1
                    if next_row_index < len(row_index_list):
                        next_row_key = row_index_list[next_row_index]
                        next_data = table[next_row_key][col_key]
                        if 'member' in current_data and 'member' in next_data:
                            if current_data['member'] == next_data['member']:
                                is_continue = True
                    table[row_key][col_key].update({
                        'is_continue': is_continue
                    })

        return table

    return None
