# coding=utf8
from django.apps import AppConfig


class BookingConfig(AppConfig):
    name = 'booking'
    verbose_name = u'การจอง'
