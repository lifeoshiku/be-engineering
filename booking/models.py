# coding=utf8

from django.db import models as models
from member.models import Member, StudyingTimeTransaction
from course.models import Course


class BookingActionEnum:
    BOOKED = 'BOOKED'
    CANCEL_BOOK = 'CANCEL_BOOK'
    CANCEL_LATE_LOGIN = 'CANCEL_LATE_LOGIN'
    CANCEL_DISPLAY = u'ยกเลิก'
    BOOKED_DISPLAY = u'จอง'
    CANCEL_LATE_LOGIN_DISPLAY = u'ยกเลิกเนื่องจากเข้าเรียนสาย'


BOOKING_ACTION_CHOICES = (
    (BookingActionEnum.BOOKED, BookingActionEnum.BOOKED_DISPLAY,),
    (BookingActionEnum.CANCEL_BOOK, BookingActionEnum.CANCEL_DISPLAY,),
    (BookingActionEnum.CANCEL_LATE_LOGIN, BookingActionEnum.CANCEL_LATE_LOGIN_DISPLAY,),
)


class Booking(models.Model):
    date = models.DateField(
        null=False,
        blank=False,
        db_index=True,
        verbose_name=u'วันที่จอง'
    )
    member = models.ForeignKey(
        'member.Member',
    )
    course = models.ForeignKey(
        'course.Course'
    )
    seat = models.ForeignKey(
        'app_settings.Seat',
    )
    action = models.CharField(
        max_length=32,
        null=False,
        default=BookingActionEnum.BOOKED,
        choices=BOOKING_ACTION_CHOICES
    )
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    @property
    def display_period(self):
        periods = self.bookingperiod_set.all().select_related().order_by('period__start_time')
        period_list = list(periods)
        start_period = period_list[0].period
        end_period = period_list[-1].period
        return u'%s - %s' % (start_period.start_time.strftime('%H:%M'), end_period.end_time.strftime('%H:%M'), )

    def __str__(self):
        return u'%s %s %s [%s]' % (self.course, self.date, self.display_period, self.seat)

    def __unicode__(self):
        return u'%s' % self.period

    class Meta:
        ordering = ('-created',)
        verbose_name = u'การจอง'
        verbose_name_plural = u'การจอง'


class BookingPeriod(models.Model):
    booking = models.ForeignKey(
        'booking.Booking'
    )
    period = models.ForeignKey(
        'app_settings.Period',
    )
    created = models.DateTimeField(auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(auto_now=True, editable=False)

    def __str__(self):
        return u'%s' % self.period

    def __unicode__(self):
        return u'%s' % self.period

    class Meta:
        ordering = ('period__start_time',)
        verbose_name = u'เวลาการจอง'
        verbose_name_plural = u'เวลาการจอง'
        unique_together = ('booking', 'period')


class StudyingTimeTransactionProxy(StudyingTimeTransaction):

    class Meta:
        proxy = True
        ordering = ('-created',)
        verbose_name = u'สถานะใช้เวลาเรียน'
        verbose_name_plural = verbose_name
