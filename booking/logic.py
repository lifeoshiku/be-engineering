from app_settings.models import (
    SpecialDayOff,
    SpecialDayOn,
    SpecialPeriodOfDate,
    SettingProfile,
    Seat,
)
from .models import (
    Booking,
    BookingPeriod,
    BookingActionEnum,
)
from .exceptions import (
    AlreadyBooked,
    SlotNotAvailable,
    NotEnoughStudyTime,
)
from member.models import (
    StudyingTimeTransaction,
    CourseMember,
    TransactionActionEnum,
)
import datetime


def get_time_diff(t1, t2):
    today = datetime.date.today()
    dt1 = datetime.datetime.combine(today, t1)
    dt2 = datetime.datetime.combine(today, t2)

    return dt1 - dt2

def sum_period_to_time(period_list):
    sum = datetime.timedelta()
    for period in period_list:
        duration = get_time_diff(period.end_time, period.start_time)
        sum = sum + duration
    return sum

def get_hour_from_timedelta(td):
    hours, reminder = divmod(td.seconds, 3600)

    return hours


def get_available_seat(date, period, default_seat):
    booked_seats = BookingPeriod.objects.filter(
        booking__date=date,
        period=period,
        booking__action=BookingActionEnum.BOOKED,
    ).select_related()

    all_booked_seat_id = [bs.booking.seat.id for bs in booked_seats]
    available_seats = [s for s in default_seat if s.id not in all_booked_seat_id]

    return available_seats

def is_special_day_off(date):
    valid = True
    try:
        SpecialDayOff.objects.get(
            start_date__lte=date,
            end_date__gte=date,
            public=True,
        )
    except SpecialDayOff.DoesNotExist:
        valid = False

    return valid


def is_special_day_on(date):
    valid = True
    try:
        SpecialDayOn.objects.get(
            start_date__lte=date,
            end_date__gte=date,
            public=True,
        )
    except SpecialDayOn.DoesNotExist:
        valid = False

    return valid


def is_default_day(date):
    setting_profile = SettingProfile.get_default()
    # python start day of week is Monday
    available_day_in_week = [
        setting_profile.monday,
        setting_profile.tuesday,
        setting_profile.wednesday,
        setting_profile.thursday,
        setting_profile.friday,
        setting_profile.saturday,
        setting_profile.sunday,
    ]
    weekday = date.weekday()
    return available_day_in_week[weekday]


def get_special_periods(date):
    try:
        special_date = SpecialPeriodOfDate.objects.get(
            start_date__lte=date,
            end_date__gte=date,
            public=True,
        )
        periods = special_date.specialperiodofdatelineitem_set.filter(public=True)
    except SpecialPeriodOfDate.DoesNotExist:
        return []

    return [p.period for p in periods]


def get_default_periods(date):
    setting_profile = SettingProfile.get_default()
    periods = setting_profile.get_periods(date)

    return [p.period for p in periods]


def generate_booking_slot(periods, duration):
    slots = []

    sorted_period = sorted(periods, key=lambda p: p.start_time)
    for idx, value in enumerate(sorted_period):
        next_idx = idx
        start_time = sorted_period[idx].start_time
        end_time = sorted_period[next_idx].end_time
        slot_periods = []
        slot_periods.append(sorted_period[next_idx])
        time_diff = get_time_diff(end_time, start_time)

        while time_diff <= duration:
            period_duration = sum_period_to_time(slot_periods)
            if time_diff == duration:
                if period_duration == duration:
                    slots.append({
                        'start_time': start_time,
                        'end_time': end_time,
                        'periods': slot_periods,
                    })
                break
            else:
                next_idx = next_idx + 1
                if next_idx == len(sorted_period):
                    break
                slot_periods.append(sorted_period[next_idx])
                end_time = sorted_period[next_idx].end_time
                time_diff = get_time_diff(end_time, start_time)

    return slots


def convert_period_to_list(period):
    result = []
    for p in period:
        result.append({
            'id': p.id,
            'start_time': p.start_time,
            'end_time': p.end_time
        })

    return result


# require setting.Period object
def filter_out_pass_time_period(periods):
    setting_profile = SettingProfile.get_default()
    late_booking = datetime.timedelta(
        minutes=setting_profile.late_booking_minute
    )

    result = []
    today = datetime.date.today()
    now = datetime.datetime.now()

    for p in periods:
        start_datetime = datetime.datetime.combine(today, p.start_time)
        if start_datetime + late_booking >= now:
            result.append(p)

    return result


def get_booked_period_ids(date, course, member):
    booked_period = BookingPeriod.objects.filter(
        booking__date=date,
        booking__member=member,
        booking__action=BookingActionEnum.BOOKED,
    ).select_related()

    return [bp.period.id for bp in booked_period]


def search_booking_slot(date=None, course=None, duration=None, member=None):
    if not date or not course or not duration:
        raise Exception('date, course, member and duration are required')

    if type(date) != datetime.date:
        raise Exception('date should be datetime.date object')

    if type(duration) != datetime.timedelta:
        raise Exception('duration should be datetime.timedelta object')

    if is_special_day_off(date):
        return None

    if is_special_day_on(date) or is_default_day(date):
        special_periods = get_special_periods(date)
        default_periods = get_default_periods(date)
        periods = special_periods or default_periods

        if date == datetime.date.today():
            periods = filter_out_pass_time_period(periods)

        if member:
            booked_period_ids = get_booked_period_ids(date, course, member)
            available_periods = [p for p in periods if p.id not in booked_period_ids]
        else:
            available_periods = periods

        slots = generate_booking_slot(available_periods, duration)

        result = []
        for slot in slots:
            periods = slot['periods']
            available_seats = find_match_seat_in_period(date, periods)

            if available_seats:
                result.append({
                    'start_time': slot['start_time'],
                    'end_time': slot['end_time'],
                    'period_id_list': [p.id for p in periods],
                })

        return result

    return None


def find_match_seat_in_period(date, period_list):
    available_seats = None

    seats = Seat.objects.filter(public=True).order_by('name')
    default_seat = [s for s in seats]

    for period in period_list:
        period_seats = get_available_seat(date, period, default_seat)
        if available_seats is None:
            available_seats = period_seats
        else:
            period_seat_id_list = [ps.id for ps in period_seats]
            available_seats = [s for s in available_seats if s.id in period_seat_id_list]

    return available_seats


def has_booked(date, period_list, member):
    period_id_list = [p.id for p in period_list]
    booked = BookingPeriod.objects.filter(
        booking__date=date,
        period_id__in=period_id_list,
        booking__member=member,
        booking__action=BookingActionEnum.BOOKED
    )

    if booked.count() > 0:
        return False

    return True


def book_slot(date, course, period_list, member):
    if not member or not course or not period_list:
        raise Exception('date, member, course, and period list is required')

    if type(period_list) is not list:
        raise Exception('period list must be a list of period object')

    if type(date) is not datetime.date:
        raise Exception('date should be datetime.date object')

    if not has_booked(date, period_list, member):
        raise AlreadyBooked()

    available_seats = find_match_seat_in_period(date, period_list)
    if not available_seats:
        raise SlotNotAvailable()

    try:
        course_member = CourseMember.objects.get(
            member=member,
            course=course,
        )
    except CourseMember.DoesNotExist:
        msg = 'member {0} not register course {1}'.format(member, course)
        raise Exception(msg)

    start_time = period_list[0].start_time
    end_time = period_list[-1].end_time
    time_diff = get_time_diff(end_time, start_time)
    studying_hours = -get_hour_from_timedelta(time_diff)

    if course_member.remaining_studying_hours + studying_hours < 0:
        raise NotEnoughStudyTime()

    seat = available_seats[0]
    booking = Booking(
        date=date,
        member=member,
        course=course,
        seat=seat
    )
    booking.save()

    for period in period_list:
        booking_period = BookingPeriod(
            booking=booking,
            period=period,
        )
        booking_period.save()
        booking.bookingperiod_set.add(booking_period)

    transaction = StudyingTimeTransaction(
        member=member,
        course=course_member,
        studying_hours=studying_hours,
        action=TransactionActionEnum.BOOKING,
        reason=None,
    )
    transaction.save()

    return booking


def is_free_cancel_booking(booking):
    if type(booking) == int:
        periods = BookingPeriod.objects.filter(booking_id=booking)
    else:
        periods = BookingPeriod.objects.filter(booking=booking)

    periods = periods.prefetch_related('booking').order_by('period__start_time')
    period = periods.first()
    start_time = period.period.start_time
    start_date = period.booking.date
    start_datetime = datetime.datetime.combine(start_date, start_time)

    setting = SettingProfile.get_default()
    time_diff = datetime.timedelta(minutes=setting.minute_before_cancel)

    now = datetime.datetime.now()
    return start_datetime - now >= time_diff


def cancel_booking_slot(booking, member):
    if type(booking) == int:
        try:
            booking = Booking.objects.get(pk=booking)
        except Booking.DoesNotExist:
            return None

    booking.action = BookingActionEnum.CANCEL_BOOK
    booking.save()

    if is_free_cancel_booking(booking):
        try:
            course_member = CourseMember.objects.get(
                member=member,
                course=booking.course,
            )
        except CourseMember.DoesNotExist:
            msg = 'member {0} not register course {1}'.format(member, course)
            raise Exception(msg)

        period_list = booking.bookingperiod_set.all().order_by('period__start_time')
        start_time = period_list.first().period.start_time
        end_time = period_list.last().period.end_time
        time_diff = get_time_diff(end_time, start_time)
        studying_hours = get_hour_from_timedelta(time_diff)

        transaction = StudyingTimeTransaction(
            member=member,
            course=course_member,
            studying_hours=studying_hours,
            action=TransactionActionEnum.CANCEL_BOOKING,
            reason=None,
        )
        transaction.save()

    return True


def get_current_period(dt):
    setting_profile = SettingProfile.get_default()
    periods = setting_profile.get_periods(dt.date())

    for period in periods:
        p = period.period
        if p.start_time <= dt.time() < p.end_time:
            return p

    return None
