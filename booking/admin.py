# coding=utf8

from django.contrib import admin
from django import forms
from .models import Booking, BookingPeriod, StudyingTimeTransactionProxy
from member.models import TransactionActionEnum, Member
from custom_admin.sites import site
from dal import autocomplete


class BookingAdminForm(forms.ModelForm):

    class Meta:
        model = Booking
        fields = '__all__'


class BookingPeriodInlineAdmin(admin.TabularInline):
    model = BookingPeriod
    fields = ['period', 'created', 'last_updated',]
    readonly_fields = ['created', 'last_updated',]
    extra = 0


class BookingAdmin(admin.ModelAdmin):
    form = BookingAdminForm
    list_display_links = None
    list_display = ['date', 'custom_name', 'course', 'display_period', 'seat']
    search_fields = ('member__username', 'member__first_name', 'member__last_name',
                     'member__nick_name', 'member__email', 'member__facebook')
    list_filter = ('course', )
    readonly_fields = ['created', 'last_updated']
    inlines = [BookingPeriodInlineAdmin, ]

    def custom_name(self, obj):
        member = obj.member
        return u'(%s) %s %s' % (member.nick_name or '-', member.first_name, member.last_name)

    custom_name.short_description = u'ชื่อเล่น / ชื่อจริง'
    custom_name.admin_order_field = 'nick_name'

    def has_add_permission(self, request):
        return False

site.register(Booking, BookingAdmin)


class StudyingTimeTransactionForm(forms.ModelForm):
    ACTION_CHOICES = (
        ('+', u'เพิ่มชั่วโมง', ),
        ('-', u'ลดชั่วโมง', ),
    )

    action = forms.ChoiceField(choices=ACTION_CHOICES, label=u'เพิ่ม/ลด')
    studying_hours = forms.IntegerField(label=u'ชั่วโมงเรียน (มากกว่า 0)', min_value=1)

    def clean_studying_hours(self):
        action = self.data['action']
        studying_hours = self.cleaned_data['studying_hours']
        course = self.cleaned_data['course']
        if studying_hours <= 0:
            raise forms.ValidationError(u'จำนวนชั่วโมงต้องมากกว่า 0')

        if action == '-':
            if course.remaining_studying_hours - studying_hours < 0:
                raise forms.ValidationError(u'ไม่สามารถลดชั่วโมงเรียนได้เนื่องจาก จำนวนชั่วโมงคงเหลือน้อยกว่าจำนวนที่ต้องการลด')
            studying_hours = -studying_hours
        
        return studying_hours

    def clean_action(self):
        action = self.data['action']
        if action == '-':
            action = TransactionActionEnum.MANUAL_DECREASE
        elif action == '+':
            action = TransactionActionEnum.MANUAL_INCREASE

        return action

    class Meta:
        model = StudyingTimeTransactionProxy
        fields = '__all__'
        widgets = {
            'member': autocomplete.ModelSelect2(
                url='member-autocomplete',
                attrs={
                    'data-placeholder': u'พิมพ์เพื่อค้นหา',
                    'data-minimum-input-length': 3,
                },
            ),
            'course': autocomplete.Select2(
                url='course-member-autocomplete',
                forward=('member', ),
                attrs={
                    'data-placeholder': u'พิมพ์เพื่อค้นหา',
                    'data-minimum-input-length': 0,
                },
            )
        }


class StudyingTimeTransactionAdmin(admin.ModelAdmin):
    model = StudyingTimeTransactionProxy
    form = StudyingTimeTransactionForm
    actions = None
    list_display_links = None
    list_display = ('member', 'course', 'abs_studying_hours', 'action', 'created', )
    readonly_fields = ('created', 'abs_studying_hours', )
    search_fields = ('member__username', 'member__first_name', 'member__last_name',
                     'member__nick_name', 'member__email', 'member__facebook',)
    list_filter = ('course__course', 'action', )

    fieldsets = (
        (None, {
            'fields': (
                'member', 'course', 'studying_hours', 'action', 'reason',
            )
        }),
    )

    def abs_studying_hours(self, obj):
        return abs(obj.studying_hours)

    abs_studying_hours.short_description = u'ชั่วโมงเรียน'

    def has_change_permission(self, request, obj=None):
        if not obj:
            return True

        return False

    def has_delete_permission(self, request, obj=None):
        return False


site.register(StudyingTimeTransactionProxy, StudyingTimeTransactionAdmin)
