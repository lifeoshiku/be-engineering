class SlotNotAvailable(Exception):
    pass


class NotEnoughStudyTime(Exception):
    pass


class AlreadyBooked(Exception):
    pass