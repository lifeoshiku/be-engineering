from django.test import TestCase
from app_settings.tests import create_default_settings
from .logic import *
from .exceptions import AlreadyBooked, SlotNotAvailable, NotEnoughStudyTime
from app_settings.models import (
    SpecialDayOff,
    SpecialDayOn,
    SpecialPeriodOfDate,
    SpecialPeriodOfDateLineItem,
    SettingProfile,
    Period,
    Seat,
    Department,
)
from member.models import (
    Member,
    CourseMember,
    StudyingTimeTransaction,
    TransactionActionEnum,
)
from course.models import Course
import datetime


class BookingTestCase(TestCase):
    courses = {}
    DEFAULT_COURSE_HOUR = 30

    def setupDepartment(self):
        self.department = Department(
            name='sample department'
        )
        self.department.save()

    def setupCourse(self):
        course = Course(
            name='ME101 Static',
            detail='',
            default_hours=self.DEFAULT_COURSE_HOUR,
            public=True
        )
        course.save()
        self.courses['static'] = course

        course = Course(
            name='ME102 Fluid',
            detail='',
            default_hours=self.DEFAULT_COURSE_HOUR,
            public=True
        )
        course.save()
        self.courses['fluid'] = course

        course = Course(
            name='ME103 Dynamic',
            detail='',
            default_hours=self.DEFAULT_COURSE_HOUR,
            public=True
        )
        course.save()
        self.courses['dynamic'] = course

    def generateMember(self, member_count):
        members = []
        default_password = '1234'

        for inc in range(member_count):
            username = 'student' + str(inc)
            first_name = 'first' + str(inc)
            last_name = 'last' + str(inc)
            member = Member(
                username=username,
                first_name=first_name,
                last_name=last_name,
                nick_name=first_name,
                gender='male',
                mobile='0123456789',
                email='xxx@xxx.com',
                facebook='https://www.facebook.com/xxx',
                address='',
                picture=None,
                department=self.department,
                gpax='3.0',
                known_form='',
            )
            member.set_password(default_password)
            member.save()

            for subject in ['static', 'fluid', 'dynamic']:
                course = CourseMember(
                    member=member,
                    course=self.courses[subject],
                )
                course.save()
                member.coursemember_set.add(course)
            members.append(member)

        return members

    def setUp(self):
        create_default_settings()
        self.setupCourse()
        self.setupDepartment()

    def _check_duration_of_booking_slot_result(self, result, duration):
        dummy_date = datetime.datetime.today()
        for r in result:
            start_time = r['start_time']
            end_time = r['end_time']

            start_datetime = datetime.datetime.combine(dummy_date, start_time)
            end_datetime = datetime.datetime.combine(dummy_date, end_time)
            time_diff = end_datetime - start_datetime
            self.assertEqual(time_diff, duration)

    def test_001_search_in_off_day_should_return_None(self):
        tomorrow = datetime.date.today() + datetime.timedelta(days=1)
        day_off = SpecialDayOff(
            start_date=tomorrow,
            end_date=tomorrow,
            public=True
        )
        day_off.save()

        result = search_booking_slot(tomorrow, self.courses['static'], datetime.timedelta(hours=1))
        self.assertEqual(result, None)

    def test_002_search_in_not_default_day_should_return_None(self):
        setting_profile = SettingProfile.get_default()
        setting_profile.friday = False
        setting_profile.save()

        friday = datetime.date(2018, 7, 6) # Friday
        result = search_booking_slot(friday, self.courses['static'], datetime.timedelta(hours=1))
        self.assertEqual(result, None)

    def test_003_duration_of_all_booking_slot_should_equal_to_passing_duration(self):
        date = datetime.date(2018, 7, 6) # Friday

        duration = datetime.timedelta(hours=1)
        result = search_booking_slot(date, self.courses['static'], duration)
        self._check_duration_of_booking_slot_result(result, duration)

        duration = datetime.timedelta(hours=2)
        result = search_booking_slot(date, self.courses['static'], duration)
        self._check_duration_of_booking_slot_result(result, duration)

        duration = datetime.timedelta(hours=3)
        result = search_booking_slot(date, self.courses['static'], duration)
        self._check_duration_of_booking_slot_result(result, duration)

    def test_004_search_today_should_return_period_greater_than_current_time(self):
        today = datetime.date.today()
        now = datetime.datetime.now()
        duration = datetime.timedelta(hours=1)
        result = search_booking_slot(today, self.courses['static'], duration)

        for r in result:
            d = datetime.datetime.combine(today, r['start_time'])
            self.assertEqual(d > now, True)

    def test_005_search_booking_result_should_not_return_booked_slot(self):
        seats = Seat.objects.filter(public=True).update(public=False)
        seats = Seat.objects.first()
        seats.public = True
        seats.save()

        # booking 1 hour
        member_count = 10
        members = self.generateMember(member_count)

        tomorrow = datetime.date.today() + datetime.timedelta(days=1)
        course = self.courses['static']
        duration_2hrs = datetime.timedelta(hours=2)
        duration_1hr = datetime.timedelta(hours=1)

        slots = search_booking_slot(tomorrow, course, duration_1hr, members[0])
        before_book_slot_length = len(slots)
        for idx in range(0, member_count, 1):
            period_id_list = slots[idx]['period_id_list']
            period_list = [Period.objects.get(id=pid) for pid in period_id_list]
            member = members[idx]
            seat = book_slot(tomorrow, course, period_list, member)

        slots = search_booking_slot(tomorrow, course, duration_1hr, members[0])
        after_book_slot_length = len(slots)

        self.assertEqual(before_book_slot_length - member_count, after_book_slot_length)

        # booking 2 hour
        next2day = datetime.date.today() + datetime.timedelta(days=2)

        slots = search_booking_slot(next2day, course, duration_1hr, members[0])
        all_slot_length = len(slots)
        setting_profile = SettingProfile.get_default()
        periods_in_date = setting_profile.get_periods(next2day)
        self.assertEqual(all_slot_length, len(periods_in_date))

        for idx in range(0, 5, 1):
            member = members[idx]
            slots = search_booking_slot(next2day, course, duration_2hrs, member)
            period_id_list = slots[0]['period_id_list']
            period_list = [Period.objects.get(id=pid) for pid in period_id_list]
            seat = book_slot(next2day, course, period_list, member)

        slots = search_booking_slot(next2day, course, duration_1hr, members[0])
        after_book_slot_length = len(slots)

        self.assertNotEqual(after_book_slot_length, all_slot_length)

    def test_006_search_2_hour_booking_should_return_None_if_alreay_booked_every_one_hour_skip_one_hour(self):
        seats = Seat.objects.filter(public=True).update(public=False)
        seats = Seat.objects.first()
        seats.public = True
        seats.save()

        tomorrow = datetime.date.today() + datetime.timedelta(days=1)
        course = self.courses['static']
        duration_2hrs = datetime.timedelta(hours=2)
        duration_1hr = datetime.timedelta(hours=1)

        slots = search_booking_slot(tomorrow, course, duration_1hr)
        member_count = len(slots)
        members = self.generateMember(member_count)
        for idx in range(0, member_count, 2):
            period_id_list = slots[idx]['period_id_list']
            period_list = [Period.objects.get(id=pid) for pid in period_id_list]
            member = members[idx]
            seat = book_slot(tomorrow, course, period_list, member)
        slots = search_booking_slot(tomorrow, course, duration_2hrs, member)
        self.assertEqual([], slots)

    def test_007_search_booking_should_return_None_if_all_slot_has_already_booked(self):
        tomorrow = datetime.date.today() + datetime.timedelta(days=1)
        course = self.courses['static']
        duration_1hr = datetime.timedelta(hours=1)
        seats = Seat.objects.filter(public=True)
        slots = search_booking_slot(tomorrow, course, duration_1hr)
        members = self.generateMember(len(slots) * len(seats))

        memberIdx = 0
        for idx, slot in enumerate(slots):
            period_id_list = slot['period_id_list']
            period_list = [Period.objects.get(id=pid) for pid in period_id_list]

            for seatIdx in range(0, len(seats), 1):
                member = members[memberIdx]
                memberIdx = memberIdx + 1
                booked = book_slot(tomorrow, course, period_list, member)
                self.assertNotEqual(booked, None)

        slots = search_booking_slot(tomorrow, course, duration_1hr, members[0])
        self.assertEqual(slots, [])

    def test_008_search_booking_on_special_day_on_should_return_booking_result(self):
        setting_profile = SettingProfile.get_default()
        setting_profile.friday = False
        setting_profile.saturday = False
        setting_profile.sunday = False
        setting_profile.save()

        friday = datetime.date(2018, 7, 6) # Friday
        saturday = friday + datetime.timedelta(days=1)
        sunday = saturday + datetime.timedelta(days=1)
        day_on = SpecialDayOn(
            start_date=friday,
            end_date=saturday,
            public=True,
        )
        day_on.save()

        course = self.courses['static']
        duration_1hr = datetime.timedelta(hours=1)
        members = self.generateMember(1)
        member = members[0]

        slots = search_booking_slot(friday, course, duration_1hr, member)
        self.assertEqual(len(slots), len(setting_profile.get_periods(friday)))
        slots = search_booking_slot(saturday, course, duration_1hr, member)
        self.assertEqual(len(slots), len(setting_profile.get_periods(saturday)))

        slots = search_booking_slot(sunday, course, duration_1hr, member)
        self.assertEqual(slots, None)

    def test_009_search_booking_on_special_period_date_should_return_all_special_period_result(self):
        tomorrow = datetime.date.today() + datetime.timedelta(days=1) 
        course = self.courses['static']
        duration_1hr = datetime.timedelta(hours=1)

        spod = SpecialPeriodOfDate(
            start_date=tomorrow,
            end_date=tomorrow,
            public=True
        )
        spod.save()

        setting_profile = SettingProfile.get_default()
        default_periods = setting_profile.get_periods(tomorrow)
        special_period = default_periods[3:7]

        for period in special_period:
            spodli = SpecialPeriodOfDateLineItem(
                period=period.period,
                special_date=spod,
            )
            spodli.save()
            spod.specialperiodofdatelineitem_set.add(spodli)

        members = self.generateMember(1)
        member = members[0]
        slots = search_booking_slot(tomorrow, course, duration_1hr, member)

        self.assertEqual(len(slots), len(special_period))

    def test_010_search_booking_on_special_day_and_special_period_should_return_all_special_period_result(self):
        setting_profile = SettingProfile.get_default()
        setting_profile.friday = False
        setting_profile.saturday = False
        setting_profile.sunday = False
        setting_profile.save()

        friday = datetime.date(2018, 7, 6) # Friday
        saturday = friday + datetime.timedelta(days=1)
        sunday = saturday + datetime.timedelta(days=1)
        day_on = SpecialDayOn(
            start_date=friday,
            end_date=saturday,
            public=True,
        )
        day_on.save()

        course = self.courses['static']
        duration_1hr = datetime.timedelta(hours=1)

        spod = SpecialPeriodOfDate(
            start_date=friday,
            end_date=saturday,
            public=True
        )
        spod.save()

        default_periods = setting_profile.get_periods(saturday)
        special_period = default_periods[3:7]

        for period in special_period:
            spodli = SpecialPeriodOfDateLineItem(
                period=period.period,
                special_date=spod,
            )
            spodli.save()
            spod.specialperiodofdatelineitem_set.add(spodli)

        slots = search_booking_slot(friday, course, duration_1hr)
        self.assertEqual(len(slots), len(special_period))

    def test_011_member_remaining_studying_time_should_be_subtraced_with_booked_duration(self):
        date = datetime.date.today() + datetime.timedelta(days=1)
        course = self.courses['static']
        members = self.generateMember(1)
        member = members[0]

        duration_1hr = datetime.timedelta(hours=1)
        slots = search_booking_slot(date, course, duration_1hr, member)
        period_id_list = slots[0]['period_id_list']
        period_list = [Period.objects.get(id=pid) for pid in period_id_list]
        booked = book_slot(date, course, period_list, member)
        course_member = CourseMember.objects.get(member=member, course=course)
        self.assertEqual(course_member.remaining_studying_hours, self.DEFAULT_COURSE_HOUR -1)

        duration_2hrs = datetime.timedelta(hours=2)
        slots = search_booking_slot(date, course, duration_2hrs, member)
        period_id_list = slots[0]['period_id_list']
        period_list = [Period.objects.get(id=pid) for pid in period_id_list]
        booked = book_slot(date, course, period_list, member)
        course_member = CourseMember.objects.get(member=member, course=course)
        self.assertEqual(course_member.remaining_studying_hours, self.DEFAULT_COURSE_HOUR - 3)

        duration_3hrs = datetime.timedelta(hours=3)
        slots = search_booking_slot(date, course, duration_3hrs, member)
        period_id_list = slots[0]['period_id_list']
        period_list = [Period.objects.get(id=pid) for pid in period_id_list]
        booked = book_slot(date, course, period_list, member)
        course_member = CourseMember.objects.get(member=member, course=course)
        self.assertEqual(course_member.remaining_studying_hours, self.DEFAULT_COURSE_HOUR -6)

    def test_012_member_cannot_book_slot_if_not_enough_study_time(self):
        date = datetime.date.today() + datetime.timedelta(days=1) 
        course = self.courses['static']
        members = self.generateMember(1)
        member = members[0]
        course_member = CourseMember.objects.get(member=member, course=course)

        stt = StudyingTimeTransaction(
            member=member,
            course=course_member,
            studying_hours=-self.DEFAULT_COURSE_HOUR,
            action=TransactionActionEnum.INITIALIZE
        )
        stt.save()

        duration_1hr = datetime.timedelta(hours=1)
        slots = search_booking_slot(date, course, duration_1hr, member)
        period_id_list = slots[0]['period_id_list']
        period_list = [Period.objects.get(id=pid) for pid in period_id_list]
        try:
            booked = book_slot(date, course, period_list, member)
        except Exception as e:
            self.assertEqual(type(e), NotEnoughStudyTime)

    def test_013_member_cannot_book_slot_repeately(self):
        members = self.generateMember(1)
        member = members[0]
        date = datetime.date.today() + datetime.timedelta(days=1)
        course = self.courses['static']
        duration_1hr = datetime.timedelta(hours=1)

        slots = search_booking_slot(date, course, duration_1hr, member)
        period_id_list = slots[0]['period_id_list']
        period_list = [Period.objects.get(id=pid) for pid in period_id_list]
        booked = book_slot(date, course, period_list, member)

        slots = search_booking_slot(date, course, duration_1hr, member)
        self.assertNotEqual(period_id_list, slots[0]['period_id_list'])

    def test_014_booking_result_should_be_only_one_seat_per_period_per_day(self):
        members = self.generateMember(2)
        member1 = members[0]
        member2 = members[1]

        date = datetime.date.today() + datetime.timedelta(days=1)
        course1 = self.courses['static']
        course2 = self.courses['fluid']
        duration_1hr = datetime.timedelta(hours=1)

        # book couse 1 by member 1
        slots = search_booking_slot(date, course1, duration_1hr, member1)
        period_id_list = slots[0]['period_id_list']
        period_list = [Period.objects.get(id=pid) for pid in period_id_list]
        booked1 = book_slot(date, course1, period_list, member1)

        # book couse 2 by member 2
        slots = search_booking_slot(date, course2, duration_1hr, member2)
        period_id_list = slots[0]['period_id_list']
        period_list = [Period.objects.get(id=pid) for pid in period_id_list]
        booked2 = book_slot(date, course2, period_list, member2)

        self.assertNotEqual(booked1.seat, booked2.seat)

    def test_015_duplicated_booking_slot_should_be_false(self):
        members = self.generateMember(2)
        member = members[0]

        date = datetime.date.today() + datetime.timedelta(days=1)
        course = self.courses['static']
        duration_1hr = datetime.timedelta(hours=1)
        duration_2hr = datetime.timedelta(hours=2)

        slots_1hour = search_booking_slot(date, course, duration_1hr, member)
        slots_2hours = search_booking_slot(date, course, duration_2hr, member)

        # duplicated by same user
        period_id_list = slots_1hour[0]['period_id_list']
        period_list = [Period.objects.get(id=pid) for pid in period_id_list]
        booked = book_slot(date, course, period_list, member)
        self.assertNotEqual(booked, None)

        try:
            booked = book_slot(date, course, period_list, member)
        except Exception as e:
            self.assertEqual(type(e), AlreadyBooked)

        # duplicated by other user with available seat
        member2 = members[1]
        booked = book_slot(date, course, period_list, member2)
        self.assertNotEqual(booked, None)

        # duplicated with intersected slot.
        period_id_list = slots_2hours[0]['period_id_list']
        period_list = [Period.objects.get(id=pid) for pid in period_id_list]
        try:
            booked = book_slot(date, course, period_list, member)
        except Exception as e:
            self.assertEqual(type(e), AlreadyBooked)


class BookingLogicUnitTestCase(TestCase):

    def setUp(self):
        create_default_settings()

    def get_current_period_should_not_none(self, dt):
        from booking.logic import get_current_period

        result = get_current_period(dt)
        self.assertNotEqual(result, None)
        self.assertEqual(True, result.start_time <= dt.time() < result.end_time)

    def test_001_get_current_period(self):
        date = datetime.date(2018, 7, 2) # Monday
        setting_profile = SettingProfile.get_default()
        periods = setting_profile.get_periods(date)
        periods = [p.period for p in periods]

        for period in periods:
            hour = period.start_time.hour
            t1 = datetime.time(hour=hour, minute=0)
            t2 = datetime.time(hour=hour, minute=30)
            t3 = datetime.time(hour=hour, minute=59)

            dt1 = datetime.datetime.combine(date, t1)
            dt2 = datetime.datetime.combine(date, t2)
            dt3 = datetime.datetime.combine(date, t3)

            self.get_current_period_should_not_none(dt1)
            self.get_current_period_should_not_none(dt2)
            self.get_current_period_should_not_none(dt3)