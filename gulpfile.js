var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function() {
    gulp.src('frontend/scss/bulma.sass')
        .pipe(sass())
        .pipe(gulp.dest('frontend/static/css'))
});

gulp.task('default', ['sass'], function() {
    gulp.watch('frontend/scss/bulma.sass', ['sass']);
})