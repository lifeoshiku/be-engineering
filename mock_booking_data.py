from booking.logic import get_special_periods, get_default_periods, book_slot
import random
from datetime import date
from course.models import Course
from app_settings.models import Seat
from member.models import Member
from datetime import timedelta

d = date.today() + timedelta(days=8)
periods = get_default_periods()
seats = Seat.objects.filter(public=True)
courses = Course.objects.all()
members = Member.objects.all().order_by('-id')[:4]


for period in periods:
    for seat in seats:
        member = random.choice(members)
        course = random.choice(courses)
        booking = book_slot(d, course, [period, ], member)
        print(booking.seat, course, period)
